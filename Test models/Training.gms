
$ ontext
Wolfram
Training my GAMS skills based on https://www.just.edu.jo/~haalshraideh/GAMS_Workshop/Material/Introduction%20to%20GAMS.pdf
$offtext

Set
    i 'proportion of fuel types'    / G1*G4 /
    j 'Quality index'               / Q1*Q2/;
    

Parameters
    cost(i)                 cost of gasoline set i
        /   G1 48
            G2 42
            G3 58
            G4 46   /
            
    Quality_spec_min(j)   minimum quality of set j
        /   Q1 85
            Q2 270  /
            
    Quality_spec_max(j)   maximum quality of set j
        /   Q1 90
            Q2 280  /;
        
Table quality_val(i,j) quality indices
    Q1 Q2
G1  99 210
G2  70 335
G3  78 280
G4  91 265;


Variables

    tot_cost    total cost in $
    x(i)        fractinon of each gasoline set
    
Equations

objective               objective function to minimize cost
min_spec_cons(j)        minimum quality index for set j  
max_spec_cons(j)        maximum quality index for set j
tot_fract_cons          setting sum of fraction to 1;

******* Model definition *************************************

objective..
    tot_cost =e= sum(i,x(i)*cost(i));

min_spec_cons(j)..
    sum(i, quality_val(i,j) * x(i)) =g= quality_spec_min(j);

max_spec_cons(j)..
    sum(i,quality_val(i,j)*x(i)) =l= quality_spec_max(j);

tot_fract_cons..
    sum(i,x(i)) =e= 1;
    
x.lo(i) = 0;
x.up(i) = 1;

model   training    / all /;

solve training using lp minimizing tot_cost;
DISPLAY x.l 

    

    



        
                

    
    


    
    