$title Crop and Animal Nutrients Global Model

$set default_in 'European_CiFoS_model_data_iterfert.xlsx'

$log Command line options:
$log --workbook=filename        specify name of data workbook                                   default: --workbook=%default_in%
$log --excel=on/off             if off, gdx will be used instead of Excel+gdx                   default: --excel=on

$log --cropInOut=on/off         allow/disallow crop import and export                           default: --cropInOut=on
$log --procInOut=on/off         allow/disallow processed products import and export             default: --procInOut=on
$log --grassHQ/MQ/LQ=on/off     switch grass hq/mq/lq on or off                                 default: --grassHQ=on --grassMQ=on --grassLQ=on
$log --LUC=on/off               switch land use change (arable pasture<->non-peat) on or off    default: --LUC=off
$log --humanNutrLost            fraction of human excreta nutrition lost for fertilisation use  default: --humanNutrLost=0.5
* The following option does not fully work yet!
* Reason: it is unclear how to deal with vFertFoodWasteSumkg(C,Crop,WasteSum), the aggregated food waste components.
* $log --animalSourcedFeed=on/off switch animal-sourced feed (meat, milk, eggs, fish) on or off   default: --animalSourcedFeed=on
$log --niter=n                  set number of iterations for the aggregated values              default: --niter=1
$log --model=model              model to run (see code: set Scenarios)                          default: --model=MinLand

$ontext
Worldwide model
2019-2020
Created by Ben
Extended by Thomas
$offtext

$onsymxref

$eolcom //

****** Default settings ******

$if not set workbook $set workbook %default_in%
$setnames %workbook% path filename fileextension
$setglobal input_file %path%%filename%

$if not set excel       $set excel      on

$if not set cropInOut   $set cropInOut  on
$if not set procInOut   $set procInOut  on
$if not set grassHQ $set grassHQ "on"
$if not set grassMQ $set grassMQ "on"
$if not set grassLQ $set grassLQ "on"
$if not set LUC     $set LUC "off"
$if not set humanNutrLost $set humanNutrLost "0.5"
$if not set animalSourcedFeed $set animalSourcedFeed "on"

$if not set niter $set niter 1
$if not set model $set model "MinLand"

****** Validate settings ******

$ifi not "%excel%"     == "on" $ifi not "%excel%"     == "off" $abort ERROR: use --excel=on     or --excel=off
$ifi not "%cropInOut%" == "on" $ifi not "%cropInOut%" == "off" $abort ERROR: use --cropInOut=on or --cropInOut=off
$ifi not "%procInOut%" == "on" $ifi not "%procInOut%" == "off" $abort ERROR: use --procInOut=on or --procInOut=off
$ifi not "%grassHQ%"   == "on" $ifi not "%grassHQ%"   == "off" $abort ERROR: use --grassHQ=on   or --grassHQ=off
$ifi not "%grassMQ%"   == "on" $ifi not "%grassMQ%"   == "off" $abort ERROR: use --grassMQ=on   or --grassMQ=off
$ifi not "%grassLQ%"   == "on" $ifi not "%grassLQ%"   == "off" $abort ERROR: use --grassLQ=on   or --grassLQ=off
$ifi not "%LUC%"       == "on" $ifi not "%LUC%"       == "off" $abort ERROR: use --LUC=on       or --LUC=off
$ifi not "%animalSourcedFeed%" == "on" $ifi not "%animalSourcedFeed%" == "off" $abort ERROR: use --animalSourcedFeed=on or --animalSourcedFeed=off

****** Write settings to log ******

$log Settings:
$iftheni %excel% == "on"
  $$log >>>>> Importing data from %workbook%
  $$log >>>>> Exporting data to Excel workbook(s)
$else
  $$log >>>>> NO data import from %workbook%, using %input_file%.gdx
  $$log >>>>> NO data output to Excel workbooks, using gdx files only
$endif
$log >>>>> Import and export of crops:                  %cropInOut%
$log >>>>> Import and export of processed products:     %procInOut%
$log >>>>> GrassHQ:                                     %grassHQ%
$log >>>>> GrassMQ:                                     %grassMQ%
$log >>>>> GrassLQ:                                     %grassLQ%
$log >>>>> Land use change arable pasture<->non-peat    %LUC%
$log >>>>> humanNutrLost:                               %humanNutrLost%
*$log >>>>> Animal-sourced feed (meat, milk, eggs):      %animalSourcedFeed%
$log >>>>> Number of iterations:                        %niter%
$log >>>>> Model to solve:                              %model%

set settings "Command line settings"    /   "excel"             "%excel%"
                                            "workbook"          "%workbook%"
                                            "cropInOut"         "%cropInOut%"
                                            "procInOut"         "%procInOut%"
                                            "grassHQ"           "%grassHQ%"
                                            "grassMQ"           "%grassMQ%"
                                            "grassLQ"           "%grassLQ%"
                                            "LUC"               "%LUC%"
                                            "humanNutrLost"     "%humanNutrLost%"
*                                            "animalSourcedFeed" "%animalSourcedFeed%"
                                            "niter"             "%niter%"
                                            "model"             "%model%"   /
;

****** Set some constants ******

$set anyInOut off
$ifi "%cropInOut%" == "on" $set anyInOut on
$ifi "%procInOut%" == "on" $set anyInOut on

$setglobal CH4 "28";
$setglobal N2O "265";

$setglobal Minfert "0.9"
$setglobal Maxfert "1.2"

$setglobal ShipmentSize "24"

* Hack for writing GDX->Excel only
$if set writeonly $goto writeonly

****** Data definition and model formulation start ******

sets

Con                                              Continents
SubCon                                           Sub-continents
C                                                Countries / Austria, Netherlands, Romania /
*/Austria,Belgium,Bulgaria,Croatia,Czechia,Denmark,Estonia,Finland,France,Germany,Greece,Hungary,Ireland,Italy,Latvia,Lithuania,Luxembourg,Malta,Netherlands,Poland,Portugal,Romania,Slovakia,Slovenia,Spain,Sweden,'United Kingdom'/
Z                                                Zones
Soil                                             Soil types
LandType                                         Types of land
CZSL(C,Z,Soil,LandType)                          Existing country-zone-soil-landtype combinations
ConC(Con,C)                                      Existing continent-country combinations

AllProd                                          All products
ProcIn(AllProd)                                  Processing input products
ProcRaw(AllProd)                                 Raw material products (multiple ProcIns can be assigned to one ProcRaw)
Crop(AllProd)                                    Crops (incl Grass)
CropGrass(Crop)                                  Crops that are grass
MeatMilkEggs(AllProd)                            Animal products for processing
ProcOut(AllProd)                                 Processing output products
ProcInt(AllProd)                                 "Intermediate products (processing output products that can be used either as output or for further processing)"
ProcOutH(AllProd)                                Human food products
ProcOutCP(AllProd)                               Co-products for animal feed and fertilisation
ProcOutG(AllProd)                                Grass and silage for animal feed
FreshGrass(AllProd)                              Fresh grass output products
CaptFish(AllProd)                                "Fisheries species"
DietProd                                         "Dietary preference products"
ProdCat                                          "Product categories Crop, MeatMilkEggs, Fish, Food, Co-product, Grass, Foodwaste, HarvestWaste"
ProdFam                                          "Product families Grass, Grain, Fruit, Meat, Fish, ..."
CP(ProdCat,AllProd)                              Existing category-product pairs
FeedCP(C,ProdCat,AllProd)                        All category-products pairs with feed loss by country
FP(ProdFam,AllProd)                              Existing family-product pairs
ProdDietMap(AllProd,DietProd)                    "Maps products and dietary preference products"
AniProdFam                                       "Feed families providing nutrition to animals"
AniProdFamProd(AniProdFam,AllProd)               "Assigning feed products to animal feed families"

CropInC(C,Crop)                                  "Existing crops in country"
*MeatMilkEggsInC(C,MeatMilkEggs)                  "Existing MeatMilkEgg production in country"
RawUse(AllProd,ProcIn)                           "Possible ProcRaw (1st index) use, links one ProcRaw to n ProcIns"

LossType                                         Loss types

HumNutr                                          Types of nutrients required by humans
AniNutr                                          Nutrients required by animals
FertNutr(AniNutr)                                Types of fertiliser nutrients

FRVCat                                           Fertiliser replacement product categories
ConFRV                                           "'Content' and fertiliser replacement value 'FRV'"

MinMax                                           The strings min and max

APS                                              Animal production systems
APSlevel                                         APS levels
HerdType                                         Herd types
APSAPSLevel(APS,APSLevel)                        Existing APS-Level combinations
APSLevelHerdType(APS,APSLevel,HerdType)          Existing APS-Level-HerdType combinations

EFSetH                                           "Housing emission factors"
EFSetC                                           "Crop emission factors"
GHG                                              "Green house gases"
Ferttype                                         "Fertiliser types for crop emission factors"

LUCCat                                           "Land use change categories"

CropOthers                                       "Spans parameter with misc information about crops like DM, Residue yield fraction, N content"
RotCrop(Crop)                                    "Subset of annual crops used for the crop rotation"
;

singleton sets

PHLoss(LossType)                                 Post harvest loss
PPLoss(LossType)                                 Processing and packaging loss
DLoss(LossType)                                  Distribution loss
CLoss(LossType)                                  Consumption loss
FLoss(LossType)                                  Feeding loss
;

alias(C,C1);
*alias(FamH,FamH1);
alias(HerdType,HerdType1);
alias(Crop,Crop1);

parameters

Population              (C)                                             Population by country
Distance                (C,C)                                           "Distance between countries in km"
LandAreaha              (LandType,Soil,Z,C)                             "Available land in ha by land type, soil type, zone, and country" // changed dims in new Excel!
CropYieldha             (LandType,Soil,Z,Crop,C)                        "Crop yield in tons per ha by land type, soil type, crop, country, and zone" // changed dims in new Excel!
CropOther               (Crop,CropOthers)                               "Misc information about crops like DM, Residue yield fraction, N content"
CropRot                 (Crop,C,Z)                                      "Crop rotation: max % of land use for specific crops by country and zone" // no data in new Excel
CropResidue             (Crop)
CropFertkg              (LandType,Soil,Z,Crop,FertNutr,C)               Fertiliser requirements of each crop in kg per ha
FRV                     (FRVCat,*,FertNutr,ConFRV)                           "Mineral fertiliser replacement values (by fertilisation category and by product/APS)" // consider * -> AllProd
CaptFishYieldFixed      (AllProd,C)                                     "Fixed yield of captured fisheries in tons"
AniNutrByProduct        (ProdCat,AllProd,AniNutr)                             Nutrients in animal feed per kg DM
AniNutrFromGrass        (AllProd,AniNutr,Con)                           "Nutrients in grass animal feed per kg (grass DM=1)"
AniNutrGrass            (C,AllProd,AniNutr)
AniNutrReq              (APS,APSLevel,HerdType,C,AniNutr)               Animal nutrient requirements
HerdTypePerPU           (APS,APSLevel,HerdType,C)                       Herd types per producer
MaxHaPerAnimal          (APS,APSLevel,HerdType,C)                       Maximum area from which an animal can feed from grazing in ha
AnimalInfo              (APS,APSLevel,HerdType,Con,*)                   "Dummy parameter for extracting AniNutrReq, HerdTypePerPU, and max ha"
AniYield                (APS,APSLevel,HerdType,Con,MeatMilkEggs)        Animal yield meat milk eggs in kg // changed dims in new Excel: continent instead of country
AniNPKRet               (APS,APSLevel,HerdType,Con,AniNutr)             NPK retained in animals // changed dims in new Excel: continent instead of country

HumNutrReq              (MinMax,HumNutr)                                "Daily min/max human nutritional requirements"
HumDiet                 (MinMax,ProdFam)                                "Human diet: min/max daily intake in grams by product family"
DietPreference          (DietProd,C)                                    "Countries' dietary preference"
HumNutrByProduct        (AllProd,HumNutr)                               "Human nutrition provided per 100g of product"
HumNutrLost             (Con)                                           "Fraction of lost nutrients from humans limiting reutilization"
Human_NPK               (FertNutr)                                      "NPK retained in the body"

Processing              (AllProd,AllProd)                               "Processing matrix (ProcIn,ProcOut)"
LossFraction            (ProdCat,AllProd,C,LossType)                    Loss fractions
LossFractionCon         (ProdCat,AllProd,Con,LossType)                  Loss fractions // changed dims in new Excel: continent instead of country
EFH                     (C,APS,EFSetH<)                                 "Emission factors for housing"
EFC                     (C,Ferttype<,EFSetC<)                           "Emission factors for crops"
GHGequiv                (GHG<)                                          "GHG emission conversion factors"
EFTransport             (*)                                             "Transportation emissions in kg CO2/CH4/N2O per 1000km per %ShipmentSize% tons"
LUC                     (LUCCat,LUCCat)                                 "Land use change from/to in tons CO2eq/ha"
;

****** Read data from Excel into gdx file ******

$onecho > options.tmp
input=%input_file%.xlsx output=%input_file%.gdx
trace = 1
MaxDupeErrors = 100
cMerge = 1
dset=Con                rng=Population!A2               rdim=1
*dset=SubCon             rng=NUTS_Names!B2               rdim=1
dset=C                  rng=Land_ha!D1               cdim=1
dset=Z                  rng=Land_ha!C2                  rdim=1
* set=CZ                 rng=NUTS_Names!C2               rdim=2 values=noData
 set=ConC               rng=Population!A2               rdim=2 values=noData
dset=LandType           rng=Land_ha!A2                  rdim=1
dset=Soil               rng=Land_ha!B2                  rdim=1
dset=ProcRaw            rng=Processing_Fraction!A2:A400 rdim=1  
dset=ProcIn             rng=Processing_Fraction!B2:B400 rdim=1 
dset=ProcOut            rng=Processing_Fraction!C2:C400 rdim=1 
 set=RawUse             rng=Processing_Fraction!A2:B400 rdim=2 values=noData 
dset=Crop               rng=Crop_Yield!D3               rdim=1
dset=MeatMilkEggs       rng=Animal_Yield!D2             cdim=1
dset=CaptFish           rng=Captured_fisheries!A2       rdim=1
 set=DietProd           rng=Diet_Preference!A6          rdim=1 values=noData
** set=CCR                 rng=NUTS_Names!D2           rdim=3 values=noData
dset=ProdCat            rng=Loss_Fraction_New!A3        rdim=1
dset=ProdFam            rng=Loss_Fraction_New!B3        rdim=1
 set=FP                 rng=Loss_Fraction_New!B3        rdim=2 values=noData ignoreColumns=C,D
 set=ProdDietMap        rng=Data_Map!B2                 rdim=2 values=noData
 set=CP                 rng=Loss_Fraction_New!A3        rdim=2 values=noData ignoreColumns=B,C,D
**dset=CropFam             rng=EUCrop_Yield!D2         cdim=1
dset=FertNutr            rng=Crop_Fert!E3               rdim=1
** set=TFC                 rng=EUCrop_Yield!D1         cdim=3 values=noData
dset=HumNutr             rng=Human_Nutr!F3              cdim=1
** set=CatFamProd          rng=Loss_Fraction!A2        rdim=3 values=noData
dset=LossType           rng=Loss_Fraction_New!D2        cdim=1
dset=MinMax             rng=Human_Nutr_Req!B3           rdim=1
dset=FRVCat             rng=FRV!A3                      rdim=1
dset=ConFRV             rng=FRV!C2                      cdim=1
dset=CropOthers         rng=crop_other!B1               cdim=1

 par=Population         rng=Population!B2               rdim=1
 par=Distance           rng=Transportation!C3           rdim=1 cdim=1
 par=LandAreaha         rng=Land_ha!A1                  rdim=3 cdim=1
 par=CropYieldha        rng=Crop_Yield!A1               rdim=4 cdim=1 ignoreRows=2
 par=CropOther          rng=crop_other!A1               rdim=1 cdim=1 
 par=CropFertkg         rng=Crop_Fert!A1                rdim=5 cdim=1 ignoreRows=2
 par=HumNutrReq         rng=Human_Nutr_Req!B2           rdim=1 cdim=1
 par=HumDiet            rng=Human_Diet_Con!B1           rdim=1 cdim=1
 par=DietPreference     rng=Diet_Preference!A5          rdim=1 cdim=1
 par=HumNutrByProduct   rng=Human_Nutr!E3               rdim=1 cdim=1
 par=Human_NPK          rng=Human_NPK!A1                rdim=0 cdim=1
 par=Processing         rng=Processing_Fraction!B2:C400 rdim=2
 par=LossFractionCon    rng=Loss_Fraction_New!A1        rdim=2 cdim=2 ignoreColumns=B,C,D

dset=APS                rng=Animal_Nutr_Req!A3          rdim=1
dset=APSlevel           rng=Animal_Nutr_Req!B3          rdim=1
dset=HerdType           rng=Animal_Nutr_Req!C3          rdim=1
 set=APSAPSLevel        rng=Animal_Nutr_Req!A3          rdim=2 values=noData
 set=APSLevelHerdType   rng=Animal_Nutr_Req!A3          rdim=3 values=noData
 par=AniYield           rng=Animal_Yield!A1             rdim=3 cdim=2
 par=AniNPKRet          rng=Animal_NPK!A1               rdim=3 cdim=2
dset=AniNutr            rng=Animal_Nutr!F2              cdim=1
 par=AnimalInfo         rng=Animal_Nutr_Req!A1          rdim=3 cdim=2
 par=AniNutrByProduct   rng=Animal_Nutr!C2              rdim=2 cdim=1 ignoreColumns=D
 par=AniNutrFromGrass   rng=Grass_Nutr!C1               rdim=2 cdim=1  ignoreColumns=D
dset=AniProdFam         rng=Animal_Nutr!D3              rdim=1
 set=AniProdFamProd     rng=Animal_Nutr!D3              rdim=2
 par=FRV                rng=FRV!A1                      rdim=2 cdim=2
 par=CaptFishYieldFixed rng=Captured_fisheries!A1       rdim=1 cdim=1
 par=EFH                rng=GHG!A1                      rdim=2 cdim=1
 par=EFC                rng=GHG!P1                      rdim=2 cdim=1
 par=GHGequiv           rng=GHG!AA2                     rdim=1
 par=EFTransport        rng=impact_transportation!C5    rdim=1
dset=LUCCat             rng=LUC_Penalty!B1              cdim=1
 par=LUC                rng=LUC_Penalty!A1              rdim=1 cdim=1
$offecho

$ifi %excel% == "on" $call GDXXRW.EXE @options.tmp

* Hack for reading Excel->GDX only
$if set readonly $exit

****** Read gdx file into GAMS model ******

$GDXIN %input_file%.gdx


// Set Imports
$LOADDCM AllProd=ProcIn AllProd=ProcOut AllProd=ProcRaw
$LOADDC Con Z Soil LandType ProcOut ProcIn ProcRaw
$LOAD RawUse  
$LOADDC Crop MeatMilkEggs CaptFish DietProd
$LOADDC ProdCat ProdFam FP CP ProdDietMap 
$LOADDC AniNutr FertNutr HumNutr LossType MinMax FRVCat ConFRV CropOthers
$LOAD ConC // no domain check possible because we don't have all country continent links
*$LOADDC C // We don't use all counties they are defined at set declaration
// Parameter Imports 
$LOAD LandAreaha CropYieldha CropFertkg Distance CaptFishYieldFixed
$LOADDC APS APSLevel HerdType APSAPSLevel APSLevelHerdType
$LOADDC HumNutrReq HumDiet GHGequiv EFTransport LossFractionCon
$LOADDC CropOther 
$LOADDC HumNutrByProduct Human_NPK Processing AniYield AniNPKRet AnimalInfo 
$LOADDC AniNutrByProduct AniNutrFromGrass AniProdFam AniProdFamProd   
$LOADDC FRV 
$LOAD LUCCat LUC EFH EFC 
$LOAD DietPreference // Someone needs to fix this manually
$LOAD Population // no domain checking possible not all countries included

$GDXIN

****** Define derived data objects ******

* ProcInt are the intermediate products which are processing output products that can undergo another processing step.
* Example: the Crop 'Rice' produces 'Rice_bran' which can either be fed to animals or further processed into 'Rice_bran_oil'.
* Defining the intermediate products is a bit tricky because the product names are not unique. Example: there is the Crop 'Rice'
* and there is the ProcOut product 'Rice'. Therefore it is important to keep track of a product's use because its name is ambiguous...
ProcInt(AllProd) = ProcRaw(AllProd) * ProcOut(AllProd) - Crop(AllProd) - MeatMilkEggs(AllProd) - CaptFish(Allprod);
*                                   ^ products that occur as input and output
*                                                        ^ less "ultimate" input (e.g., Rice-Rice, Mushroom-Mushroom, Grass_fresh-Grass_fresh, etc.)
*                                                                        ^ less "ultimate" input (e.g., Egg-Egg)
*                                                                                                ^ less "ultimate" input (e.g., Norway_lobster->Norway_lobster+Norway_lobster_FOil+Norway_lobster_FMeal)
* Also, eliminate products that cannot be produced...
ProcInt(AllProd)$(not sum(ProcIn, Processing(ProcIn,AllProd))) = no;

AniNutrReq(APSLevelHerdType,C,AniNutr)   = sum(ConC(Con,C), AnimalInfo(APSLevelHerdType,Con,AniNutr));
HerdTypePerPU(APSLevelHerdType,C)        = sum(ConC(Con,C), AnimalInfo(APSLevelHerdType,Con,'HerdTypes PerPU'));
MaxHaPerAnimal(APSLevelHerdType,C)       = sum(ConC(Con,C), AnimalInfo(APSLevelHerdType,Con,'ha'));
AniNutrGrass(C,AllProd,AniNutr)          = sum(ConC(Con,C), AniNutrFromGrass(AllProd,AniNutr,Con));
LossFraction(ProdCat,AllProd,C,LossType) = sum(ConC(Con,C), LossFractionCon(ProdCat,AllProd,Con,LossType));

CropRot(Crop,C,Z) = CropOther(Crop,'Rotation');
CropResidue(Crop) = CropOther(Crop,'Residue');

RotCrop(Crop)$CropOther(Crop,"arable")= Yes;
Alias(RotCrop, RotCrop1);


* Define ProcOutH(AllProd), human food products
ProcOutH(ProcOut)$CP('Food',ProcOut) = yes;
* Define ProcOutCP(AllProd), co-products for animal feed and fertilisation
ProcOutCP(ProcOut)$CP('Co-Product',ProcOut) = yes;
* Define ProcOutG(AllProd), grass, silage, and hay for animal feed
* and FreshGrass(AllProd), the fresh grass output products (hardcoded - ugly...)
$onEmbeddedCode Python:
ProcOutG = []
FreshGrass = []
for p in gams.get("ProcOut"):
    if p[:5] == "Grass" or p[:6] == "Silage" or p[:3] == "Hay":
        ProcOutG.append(p)
        if p[:5] == "Grass":
            FreshGrass.append(p)
gams.set("ProcOutG", ProcOutG)
gams.set("FreshGrass", FreshGrass)
$offEmbeddedCode ProcOutG FreshGrass

* Define CropGrass(Crop), crops that are grass
CropGrass(Crop)$FP('Grass',Crop) = yes;

** Define FamH(ProdFam), human food families
*FamH(ProdFam) = yes$CatFamMap('Food',ProdFam);

* Define CropInC(C,Crop), existing crops in country
CropInC(C,Crop)$sum((LandType,Soil,Z),CropYieldha(LandType,Soil,Z,Crop,C)) = yes;
* Define MeatMilkEggsInC(C,MeatMilkEggs), existing MeatMilkEgg production in country
*MeatMilkEggsInC(C,MeatMilkEggs)$sum((APS,APSLevel,HerdType),AniYield(APS,APSLevel,HerdType,C,MeatMilkEggs)) = yes;
* Define CZSL(C,Z,Soil,LandType), existing country-zone-soil-landtype combinations;
CZSL(C,Z,Soil,LandType)$LandAreaha(LandType,Soil,Z,C) = yes;

* Define loss type singleton sets (assumes certain order in LossType: Postharvest-ProcessingPackaging-Distribution-Consumption-Feeding!)
PHLoss(LossType)$(ord(LossType)=1) = yes;
PPLoss(LossType)$(ord(LossType)=2) = yes;
DLoss(LossType)$(ord(LossType)=3)  = yes;
CLoss(LossType)$(ord(LossType)=4)  = yes;
FLoss(LossType)$(ord(LossType)=5)  = yes;

* Define FeedCP(C,ProdCat,AllProd), all category-products pairs with feed loss by country
FeedCP(C,ProdCat,AllProd)$LossFraction(ProdCat,AllProd,C,FLoss) = yes;


sets

Rumi(APS)               Ruminants               / Dairy,Beef,Sheep,Goat /
Mono(APS)               Monogastrics            / Pig,Layer,Broiler /
Aquatic(APS)            Aquatic animals         / Tilapia,Salmon /
APSLand(APS)            "Land production systems" //                                /Dairy,Beef,Pig,Broiler,Layer/
Producer(APS,HerdType)  Map APS and producers   / Pig.PProducer,Dairy.DProducer,Beef.BProducer,Broiler.BrProducer
                                                  Layer.LProducer,Salmon.SProducer,Tilapia.TProducer,
                                                  Sheep.Sproducer, Goat.Gproducer /
*AniSourcedFeed(AniProdFam)  Animal-sourced feed families                            / Meat, Dairy, Egg, Fish /
AniPE(AniNutr)              Animal nutrients to meet energy protein requirements
WasteSum(LossType)          Loss types whose nutrients are aggregated               / Distribution, Consumption /
WasteByProd(LossType)       Loss types which distinguish individual products        / Postharvest, ProcessingPackaging /
ProdPHLoss(C,AllProd)       Products that have PHLoss
;

APSLand(APS) = APS(APS) - Aquatic(APS);

AniPE(AniNutr)$sum((APSLevelHerdType,C),AniNutrReq(APSLevelHerdType,C,AniNutr)) = yes;
* Take out FIC, VW, SW
AniPE('FIC') = no;
AniPE('VW')  = no;
AniPE('SW')  = no;

* The following are defined at compile time due to reporting
*WasteSum(DLoss) = yes;
*WasteSum(CLoss) = yes;
*WasteByProd(PHLoss) = yes;
*WasteByProd(PPLoss) = yes;

ProdPHLoss(C,AllProd)$sum(ProdCat, LossFraction(ProdCat,AllProd,C,PHLoss)) = yes;
ProdPHLoss(C,Crop)$(not CropInC(C,Crop)) = no;

$iftheni LUC == "on"
* Land use change: allow growing non-grass crops on Arable/pasture and grass on Arable/non-peat
CropYieldha('Arable','pasture',Z,Crop,C)$(not CropGrass(Crop))          = CropYieldha('Arable','non-peat',Z,Crop,C);
CropYieldha('Arable','non-peat',Z,CropGrass,C)                          = CropYieldha('Arable','pasture',Z,CropGrass,C);
CropFertkg('Arable','pasture',Z,Crop,FertNutr,C)$(not CropGrass(Crop))  = CropFertkg('Arable','non-peat',Z,Crop,FertNutr,C);
CropFertkg('Arable','non-peat',Z,CropGrass,FertNutr,C)                  = CropFertkg('Arable','pasture',Z,Crop,FertNutr,C);
$endif

parameter GrazingProportion         (APS)                   "Grazing proportion" / Beef=0.5,Dairy=0.5,Sheep=0.50,Goat=0.50 /
          NMineral                  (Soil)                  "Annual mineralised N from peat soils" / Peat=233.5 /
          AniNutrByProductC         (C,LossType,AniNutr)    "Animal nutrients in aggregated waste in nutrient units per kg DM"
          FeedingLossFraction       (C)                     "Feeding loss fraction for aggregated waste animal feed"
          FRVC                      (C,*,FertNutr,ConFRV)   "Mineral fertiliser replacement values and content in nutrient units per kg for aggregated waste"
;

* Initialize with average nutrient content of food waste (including zero values)
AniNutrByProductC(C,WasteSum,AniNutr) = sum(ProcOutH, AniNutrByProduct('Foodwaste',ProcOutH,AniNutr))/card(ProcOutH);
* Initialize with average feeding loss of food waste
FeedingLossFraction(C) = sum(ProcOutH, LossFraction('Foodwaste',ProcOutH,C,FLoss))/card(ProcOutH);
* Initialize with average FRV values of food waste
FRVC(C,WasteSum,FertNutr,ConFRV)  = sum(ProcOutH, FRV('Foodwaste',ProcOutH,FertNutr,ConFRV))   /card(ProcOutH);
FRVC(C,FLoss,   FertNutr,ConFRV)  = sum(ProcOutH, FRV('Foodwaste',ProcOutH,FertNutr,ConFRV))   /card(ProcOutH);

HumNutrLost(Con) = %humanNutrLost%;

****** Declare variables ******

positive variables

vNumha                          (C,Z,Soil,LandType,Crop)                        Number of hectars used
vCropAv                         (C,Crop)                                        "Total crop available on used land in tons (before any loss)"
vCropTotal                      (C,AllProd)                     "Total crop available incl. imports in tons (after postharvest loss)"
vCPTotal                        (C,AllProd)                     "Total co-product incl. imports in tons"
*vGTotal                         (C,AllProd)                     "Total grass incl. imports in tons"
vFertResiduekg                  (C,Crop,Crop)                   "Crop residues applied as fertiliser in kg"
vFertCoProductkg                (C,Crop,AllProd)                Co-products applied as fertiliser in kg
vFertManure                     (C,Crop,APS,FertNutr)           Manure applied as fertiliser in nutrient units
vFertFoodWastekg                (C,Crop,LossType,AllProd)       Food waste applied as fertiliser in kg (waste of individual products)
vFertFoodWasteSumkg             (C,Crop,LossType)               Food waste applied as fertiliser in kg (waste of aggregated products)
vFertGrazing                    (C,Crop,Rumi,FertNutr)          Manure applied while grazing in nutrient units
vFertFeedingLosskg              (C,Crop)                        Feeding losses (aggregated) applied as fertiliser in kg
vFertH                          (C,Crop,FertNutr)               Human fertiliser available in nutrient units
vFertArtificial                 (C,Crop,FertNutr)               Artificial fertiliser applied in nutrient units

vFoodWasteAv                    (C,LossType,AllProd)            Waste available in tons

vQtyIn                          (C,AllProd)                     "Input product quantity (processing matrix) in tons"
vQtyOut                         (C,AllProd)                     "Output product quantity incl. intermediate products (processing matrix) in tons"
vQtyProcOut                     (C,AllProd)                     Processing output quantity in tons
vQtyAvH                         (C,AllProd)                     Quantity of human food available after PP and D losses in tons
vConQtyH                        (C,Allprod)                     "Human food quantity consumed (local+imports) in tons"
$ifi "%cropInOut%" == "on" vFromToCrop(C,C,Crop)                "Crop transports (import/export) in tons, first index is source"
$ifi "%procInOut%" == "on" vFromTo    (C,C,AllProd)             "Food transports (import/export) in tons, first index is source"

vNumHerdType                    (C,APS,APSLevel,HerdType)                       Animal numbers per herd types
vCoProdIntakeHerdTypekgDM       (C,APS,APSLevel,HerdType,AllProd)               Intake of co-products per animal herd type in kg DM
vGrassIntakeHerdTypekgDM        (C,APS,APSLevel,HerdType,AllProd)               Intake of grass products per animal herd type in kg DM
vFoodWasteIntakeHerdTypekgDM    (C,APS,APSLevel,HerdType,LossType,AllProd)      Intake of food waste (individual products) per animal herd type in kg DM
vFoodWasteIntakeHerdTypeSumkgDM (C,APS,APSLevel,HerdType,LossType)              Intake of food waste (aggregated products) per animal herd type in kg DM

vAnimalAv                       (C,AllProd)                                     Animal products available for processing in kg
vManureAv                       (C,APS,APSLevel,HerdType,FertNutr)              Manure available
vManureM                        (C,APS,APSLevel,HerdType,FertNutr)              "Nutient excretion in manure management"
vHExAv                          (C,FertNutr)                                    Aggregate human fertiliser available

vHaGrazing                      (C,Z,Soil,LandType,Crop)                        "Maximum grazing area in C"

vCO2Transport                   (C)                                             "CO2 emissions from transportation"
vCH4Transport                   (C)                                             "CH4 emissions from transportation"
vN2OTransport                   (C)                                             "N2O emissions from transportation"

vCO2eqLUC                       (C)                                             "CO2equiv emissions from land use change"

vCO2eqCropland                  (C,Crop)                                        Total N2O-N from cropland

vTANExcretionMM                 (C,APS,APSLevel,HerdType)                       "TAN excretion from manure management"
vTANExcretionG                  (C,APS,APSLevel,HerdType)                       "TAN excretion from grazing"
vN2ONDMM                        (C,APS,APSLevel,HerdType)                       "Direct N2O N from manure management"
vNOXNMM                         (C,APS,APSLevel,HerdType)                       "Direct NOx N from manure management"
vNH3NMM                         (C,APS,APSLevel,HerdType)                       "Direct NH3 N from manure management"
vN2NMM                          (C,APS,APSLevel,HerdType)                       "Direct N2 N from manure management"
vN2ONIDMM                       (C,APS,APSLevel,HerdType)                       "Indirect N2O N from manure management"
vN2ONDeNitrM                    (C,APS,APSLevel,HerdType)                       "N2O N from aquaculture"
vN2ONDeNitrFeed                 (C,APS,APSLevel,HerdType)
vNLossesMM                      (C,APS,APSLevel,HerdType,FertNutr)              "N losses from manure management"
vVSExcretionMM                  (C,APS,APSLevel,HerdType)                       "Volatile solid excretion from manure management"
vVSExcretionG                   (C,APS,APSLevel,HerdType)                       "Volatile solid excretion from grazing"
vCH4Manure                      (C,APS,APSLevel,HerdType)                       "Methane emissions from manure"
vCH4Enteric                     (C,APS,APSLevel,HerdType)                       "Methane emissions from enteric fermentation"
vN2ONDCrop                      (C,Crop)                                        "Direct N2O N from cropland"
vNOXNCrop                       (C,Crop)                                        "Direct NOx N from cropland"
vNH3NCrop                       (C,Crop)                                        "Direct NH3 N from cropland"
vNLeachCrop                     (C,Crop)                                        "N leaching from cropland"
vN2ONLeachC                     (C,Crop)                                        "Indirect N2O from leaching"
vN2ONVolC                       (C,Crop)                                        "Indirect N2O from volatilisation"
vCO2eqCropland                  (C,Crop)                                        "Total cropland emissions"

vCO2eqAnimal                    (C,APS,APSLevel)                                Total GHG emissions from animals
vCO2eqCountry                   (C)                                             Total GHG emissions per country

;

free variables  vTotalLandUse       Total land use
                vTotalArtFert       Total artificial fertiliser
                vTotalCO2eq         Total GHG emissions
                vMaxCO2eqPerCapita  Maximum per capita GHG emissions
;

****** Land equations ******

equation eTotalLandUse "Objective function minimize land use" ;  // added a penalty for transportation to avoid circular transportation
         eTotalLandUse..
         vTotalLandUse =e= sum((CZSL(C,Z,Soil,LandType),Crop)$(CropYieldha(LandType,Soil,Z,Crop,C) and not diag('Marginal',LandType)), vNumha(CZSL,Crop))
$ifi "%anyInOut%"  == "on" + 1e-6*(
$ifi "%procInOut%" == "on"         sum((C,C1,ProcOutH)$(not diag(C,C1)),vFromTo(C,C1,ProcOutH))
$ifi "%procInOut%" == "on"         +sum((C,C1,ProcOutCP)$(not diag(C,C1)),vFromTo(C,C1,ProcOutCP))
$ifi "%CropInOut%" == "on"         +sum((C,C1,Crop)$(not diag(C,C1) and not CropGrass(Crop)),vFromToCrop(C,C1,Crop))
$ifi "%anyInOut%"  == "on"        )
;

equation eTotalArtFert "Objective function minimize artificial fertiliser";
         eTotalArtFert..
         vTotalArtFert =e= sum((C,Crop,FertNutr)$(CropInC(C,Crop) and sum((LandType,Soil,Z), CropFertkg(LandType,Soil,Z,Crop,FertNutr,C))),
                               vFertArtificial(C,Crop,FertNutr))
$ifi "%anyInOut%"  == "on" + 1e-6*(
$ifi "%procInOut%" == "on"         sum((C,C1,ProcOutH)$(not diag(C,C1)),vFromTo(C,C1,ProcOutH))
$ifi "%procInOut%" == "on"         +sum((C,C1,ProcOutCP)$(not diag(C,C1)),vFromTo(C,C1,ProcOutCP))
$ifi "%CropInOut%" == "on"         +sum((C,C1,Crop)$(not diag(C,C1) and not CropGrass(Crop)),vFromToCrop(C,C1,Crop))
$ifi "%anyInOut%"  == "on"        )
;

$macro FertUsed \
            ( \
            sum(Crop1, vFertResiduekg(C,Crop,Crop1)                                  * FRV('Residue',Crop1,FertNutr,'Content') * FRV('Residue',Crop1,FertNutr,'FRV')) + \
            sum(ProcOutCP, vFertCoProductkg(C,Crop,ProcOutCP)                        * FRV('Co-Product',ProcOutCP,FertNutr,'Content') * FRV('Co-Product',ProcOutCP,FertNutr,'FRV')) + \
            sum(APS, vFertManure(C,Crop,APS,FertNutr)                                * FRV('Manure',APS,FertNutr,'FRV')) + \
            sum(WasteSum, vFertFoodWasteSumkg(C,Crop,WasteSum)                       * FRVC(C,WasteSum,FertNutr,'Content') * FRVC(C,WasteSum,FertNutr,'FRV')) + \
            sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)), vFertFoodWastekg(C,Crop,WasteByProd,ProcOutH) * FRV('Foodwaste',ProcOutH,FertNutr,'Content')   * FRV('Foodwaste',ProcOutH,FertNutr,'FRV')) + \
            sum((WasteByProd,AllProd)$PHLoss(WasteByProd), vFertFoodWastekg(C,Crop,WasteByProd,AllProd)         * FRV('HarvestWaste',AllProd,FertNutr,'Content') * FRV('HarvestWaste',AllProd,FertNutr,'FRV')) + \
            sum(Rumi, vFertGrazing(C,Crop,Rumi,FertNutr)$CropGrass(Crop)             * FRV('Manure',Rumi,FertNutr,'FRV'))$CropGrass(Crop) + \
            vFertFeedingLosskg(C,Crop)                                               * FRVC(C,FLoss,FertNutr,'Content') * FRVC(C,FLoss,FertNutr,'FRV') + \
            vFertH(C,Crop,FertNutr) + \
            vFertArtificial(C,Crop,FertNutr) \
            )

equation eLandFertMin(C,Crop,FertNutr) "Required fertiliser" ;
         eLandFertMin(C,Crop,FertNutr)$(CropInC(C,Crop) and sum((LandType,Soil,Z), CropFertkg(LandType,Soil,Z,Crop,FertNutr,C)))..
         sum((Z,Soil,LandType)$CropYieldha(LandType,Soil,Z,Crop,C), CropFertkg(LandType,Soil,Z,Crop,FertNutr,C) * vNumha(C,Z,Soil,LandType,Crop)) * %Minfert% =l=
         FertUsed
;        

equation eLandFertMax(C,Crop,FertNutr) "Do not over-fertilise" ;
         eLandFertMax(C,Crop,FertNutr)$(CropInC(C,Crop) and sum((LandType,Soil,Z), CropFertkg(LandType,Soil,Z,Crop,FertNutr,C)))..
         sum((Z,Soil,LandType)$CropYieldha(LandType,Soil,Z,Crop,C), CropFertkg(LandType,Soil,Z,Crop,FertNutr,C) * vNumha(C,Z,Soil,LandType,Crop)) * %Maxfert% =g=
         FertUsed
;

equation eLandAvailable(C,Z,Soil,LandType) "Don't use more land than there is available" ;
         eLandAvailable(C,Z,Soil,LandType)..
         LandAreaha(LandType,Soil,Z,C) =g= sum(Crop$CropYieldha(LandType,Soil,Z,Crop,C), vNumha(C,Z,Soil,LandType,Crop))
;

*vNumha.fx(CZSL(C,Z,Soil,LandType),Crop)$(not CropYieldha(LandType,Soil,Crop,C,Z)) = 0;

equation eCropAvailable(C,Crop) "Available crop before any loss" ;
         eCropAvailable(CropInC(C,Crop))..
         vCropAv(C,Crop) =e= sum((Z,LandType,Soil)$CropYieldha(LandType,Soil,Z,Crop,C), vNumha(C,Z,Soil,LandType,Crop) * CropYieldha(LandType,Soil,Z,Crop,C))
;

** Enforce settings of the grass switch **
$ifi "%grassHQ%" == "off"
vCropAv.fx(C,'Grass_Managed_HQ')$CropInC(C,'Grass_Managed_HQ') = 0;
$ifi "%grassHQ%" == "off"
vCropAv.fx(C,'Grass_Natural_HQ')$CropInC(C,'Grass_Natural_HQ') = 0;
$ifi "%grassMQ%" == "off"
vCropAv.fx(C,'Grass_Managed_MQ')$CropInC(C,'Grass_Managed_MQ') = 0;
$ifi "%grassMQ%" == "off"
vCropAv.fx(C,'Grass_Natural_MQ')$CropInC(C,'Grass_Natural_MQ') = 0;
$ifi "%grassLQ%" == "off"
vCropAv.fx(C,'Grass_Managed_LQ')$CropInC(C,'Grass_Managed_LQ') = 0;
$ifi "%grassLQ%" == "off"
vCropAv.fx(C,'Grass_Natural_LQ')$CropInC(C,'Grass_Natural_LQ') = 0;


equation eCropRotation(Crop,C,Z,Soil,LandType) "Crop rotation: observe max land use for crops" ;
         eCropRotation(RotCrop,CZSL(C,Z,Soil,"arable"))$(CropYieldha("arable",Soil,Z,RotCrop,C) and CropRot(RotCrop,C,Z) > 1)..
         vNumha(CZSL,RotCrop) =l= sum((RotCrop1)$CropYieldha("arable",Soil,Z,RotCrop1,C), vNumha(CZSL,RotCrop1)) / CropRot(RotCrop,C,Z)
;

****** Post harvest loss equations ******

equation ePostHarvestloss(C,Crop) "Crop post harvest loss" ;
         ePostHarvestloss(CropInC(C,Crop))..
         vFoodWasteAv(C,PHLoss,Crop) =e= vCropAv(C,Crop) * LossFraction('Crop',Crop,C,PHLoss)
;

equation ePostHarvestlossMME(C,MeatMilkEggs) "MeatMilkEggs post harvest loss" ;
         ePostHarvestlossMME(C,MeatMilkEggs)..
         vFoodWasteAv(C,PHLoss,MeatMilkEggs) =e= vAnimalAv(C,MeatMilkEggs) * 0.001 * LossFraction('MeatMilkEggs',MeatMilkEggs,C,PHLoss)
;

*Captured fisheries post harvest loss
vFoodWasteAv.fx(C,PHLoss,CaptFish) = CaptFishYieldFixed(CaptFish,C) * LossFraction('Fish',CaptFish,C,PHLoss)

****** Crop import/export equations ******

$iftheni "%cropInOut%" == "on"
* Active if crop import/export is allowed
equation eOutgoingCrop(C,Crop) "Outgoing crop quantity incl. crop that stays in country";
         eOutgoingCrop(CropInC(C,Crop))..
         vCropAv(C,Crop)*(1-LossFraction('Crop',Crop,C,PHLoss)) =e= sum(C1,vFromToCrop(C,C1,Crop))$(not CropGrass(Crop))
                                                                    + vFromToCrop(C,C,Crop)$CropGrass(Crop)
;

equation eIncomingCrop(C1,Crop) "Incoming crop quantity incl. crop in same country";
         eIncomingCrop(C1,Crop)..
         vCropTotal(C1,Crop) =e= sum(C$CropInC(C,Crop),vFromToCrop(C,C1,Crop))$(not CropGrass(Crop))
                                 + vFromToCrop(C1,C1,Crop)$(CropGrass(Crop) and CropInC(C1,Crop))
;
$else
* Active if crop import/export is not allowed
equation eTotalCrop(C,Crop) "Total available crop without import/export";
         eTotalCrop(C,Crop)..
         vCropTotal(C,Crop) =e= vCropAv(C,Crop)$CropinC(C,Crop) * (1-LossFraction('Crop',Crop,C,PHLoss))
;
$endif

****** Processing equations ******

equation eRawInput(C,ProcRaw) "Link raw materials to processing input products" ;
         eRawInput(C,ProcRaw)$(not ProcInt(ProcRaw))..
         sum(ProcIn$RawUse(ProcRaw,ProcIn), vQtyIn(C,ProcIn)) =e=
*$iftheni "%cropInOut%" == "on"
* Active if crop import/export is allowed
            vCropTotal(C,ProcRaw)$Crop(ProcRaw)
*$else
* Active if crop import/export is not allowed
*            sum(Crop$CropInC(C,Crop), vCropTotal(C,ProcRaw)$Crop(ProcRaw))
*$endif
            + vAnimalAv(C,ProcRaw)$MeatMilkEggs(ProcRaw) / 1000
            + CaptFishYieldFixed(ProcRaw,C)$CaptFish(ProcRaw)
;

* The following bounds are necessary in case there are ProcRaw products that do neither have yield data nor are in MeatMilkEggs
*                                                                                                       nor are ProcInt products
*                                                                                                       nor are fishery products
* Ideally, this set should be empty...
set evilProcRaw(ProcRaw) "ProcRaw products that do neither have yield data nor are in MeatMilkEggs nor are ProcInt products nor are fishery products"
;
evilProcRaw(ProcRaw)$(not Crop(ProcRaw) and not MeatMilkEggs(ProcRaw) and not ProcInt(ProcRaw) and not CaptFish(ProcRaw)) = yes;
loop(evilProcRaw,
    vQtyIn.fx(C,ProcIn)$RawUse(evilProcRaw,ProcIn) = 0
)

equation eProcessingOutputProducts(C,ProcOut) "Processing output products balance" ;
         eProcessingOutputProducts(C,ProcOut)..
         vQtyOut(C,ProcOut) =e= sum(ProcIn, vQtyIn(C,ProcIn) * Processing(ProcIn,ProcOut))
;

equation eUseOrProcess(C,AllProd) "Processing output (vQtyOut) is either used (vQtyProcOut) or further processed as a ProcRaw if a ProcInt (vQtyIn)" ;
         eUseOrProcess(C,ProcOut)..
         vQtyOut(C,ProcOut) =e= vQtyProcOut(C,ProcOut) + sum(ProcIn$RawUse(ProcOut,ProcIn), vQtyIn(C,ProcIn))$ProcInt(ProcOut)
*                                                                   ^ (connects to ProcRaw with the same name as ProcOut if the ProcOut is a ProcInt)
;

****** Processing&Packaging and Distribution loss equations ******

equation eProcessingPackagingloss(C,AllProd) "Processing and packaging losses (food only)";
         eProcessingPackagingloss(C,ProcOutH)..
         vFoodWasteAv(C,PPLoss,ProcOutH) =e= vQtyProcOut(C,ProcOutH) * LossFraction('Food',ProcOutH,C,PPLoss)
;

equation eDistributionloss(C,AllProd) "Distribution losses (food only)";
         eDistributionloss(C,ProcOutH)..
         vFoodWasteAv(C,DLoss,ProcOutH) =e=
            vQtyProcOut(C,ProcOutH) * (1-LossFraction('Food',ProcOutH,C,PPLoss)) * LossFraction('Food',ProcOutH,C,DLoss)
;

equation eFoodAvailable(C,AllProd) "Food processing output minus processing&packaging and distribution losses";
         eFoodAvailable(C,ProcOutH)..
         vQtyAvH(C,ProcOutH) =e=
                vQtyProcOut(C,ProcOutH) - vFoodWasteAv(C,PPLoss,ProcOutH) - vFoodWasteAv(C,DLoss,ProcOutH) //- vFoodWasteAv(C,CLoss,ProcOutH)
;

****** Processed product (no grass products) import/export equations ******

$iftheni "%procInOut%" == "on"
* Active if output products' import/export is allowed
* Transport (i.e., import & export) of output products
equation eOutgoingHCP(C,AllProd) "Outgoing food and co-product quantity incl. stuff that stays in country";
         eOutgoingHCP(C,AllProd)$(ProcOutH(AllProd) or ProcOutCP(AllProd))..
         vQtyAvH(C,AllProd)$ProcOutH(AllProd) + vQtyProcOut(C,AllProd)$ProcOutCP(AllProd) =e= sum(C1,vFromTo(C,C1,AllProd))
;

equation eIncomingH(C1,AllProd) "Incoming food quantity incl. domestic food minus consumption loss";
         eIncomingH(C1,ProcOutH)..
         vConQtyH(C1,ProcOutH) =e= sum(C,vFromTo(C,C1,ProcOutH)) * (1-LossFraction('Food',ProcOutH,C1,CLoss))
;

equation eIncomingCP(C1,AllProd) "Incoming co-products incl. domestic co-products";
         eIncomingCP(C1,ProcOutCP)..
         vCPTotal(C1,ProcOutCP) =e= sum(C,vFromTo(C,C1,ProcOutCP))
;
$else
* Active if output products' import/export is not allowed
equation eConQtyH(C,AllProd) "Available food quantity minus consumption loss";
         eConQtyH(C,ProcOutH)..
         vConQtyH(C,ProcOutH) =e= vQtyAvH(C,ProcOutH) *(1-LossFraction('Food',ProcOutH,C,CLoss))
;
equation eTotalCP(C,AllProd) "Total co-product for compatibility with import/export scenario";
         eTotalCP(C,ProcOutCP)..
         vCPTotal(C,ProcOutCP) =e= vQtyProcOut(C,ProcOutCP)
;
$endif

****** Consumption loss equations ******

equation eConsumptionloss(C,AllProd) "Consumption losses (food only), calculated from domestic use + imports (if applicable)";
         eConsumptionloss(C,ProcOutH)..
         vFoodWasteAv(C,CLoss,ProcOutH) =e= vConQtyH(C,ProcOutH) * ( 1 / (1-LossFraction('Food',ProcOutH,C,CLoss)) - 1 )
;

* There is no import/export of grass products -> commented out...
*equation eOutgoingG(C,AllProd) "Outgoing grass quantity incl. stuff that stays in country";
*         eOutgoingG(C,ProcOutG)..
*         vQtyProcOut(C,ProcOutG) =e= sum(C1,vFromTo(C,C1,ProcOutG))$(not FreshGrass(ProcOutG)) + vFromTo(C,C,ProcOutG)$FreshGrass(ProcOutG)
*;
*equation eIncomingG(C1,AllProd) "Incoming grass incl. domestic grass";
*         eIncomingG(C1,ProcOutG)..
*         vGTotal(C1,ProcOutG) =e= sum(C,vFromTo(C,C1,ProcOutG))$(not FreshGrass(ProcOutG)) + vFromTo(C1,C1,ProcOutG)$FreshGrass(ProcOutG)
*;

****** Product availability equations ******

equation eCPAvailable(C,AllProd) "Available co-products and co-products to DM, used for fertilisation and/or feed" ;
         eCPAvailable(C,ProcOutCP)..
         vCPTotal(C,ProcOutCP) * 1000 =e=
                 sum(Crop$CropInC(C,Crop), vFertCoProductkg(C,Crop,ProcOutCP) ) +
                 sum(APSLevelHerdType, vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)) * 1000
                   / AniNutrByProduct('Co-Product',ProcOutCP,'DM') / (1 - LossFraction('Co-Product',ProcOutCP,C,FLoss))
;
$iftheni "%animalSourcedFeed%" == "off"
vCoProdIntakeHerdTypekgDM.fx(C,APSLevelHerdType,ProcOutCP)$sum(AniSourcedFeed, AniProdFamProd(AniSourcedFeed,ProcOutCP)) = 0;
$endif

equation eGrassAvailable(C,AllProd) "Available grass" ;  // assumes grass products (grass, silage, hay) have a DM value of 1
         eGrassAvailable(C,ProcOutG)..
         vQtyProcOut(C,ProcOutG) * 1000 =g=
                 sum(APSLevelHerdType, vGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG)) / (1 - LossFraction('Grass',ProcOutG,C,FLoss))
;

equation eResidueAvailable(C,Crop) "Available crop residues" ;
         eResidueAvailable(C,Crop)$CropInC(C,Crop)..
         vCropAv(C,Crop)*CropResidue(Crop) * 1000 =e= sum(Crop1, vFertResiduekg(C,Crop1,Crop))
;

* Ben's previous comment: I added this "FeedingCatAllProd(WasteType,AllProd)" here to allow me to use allprod but not include grass and
* co-products. WasteType is a subset of feedingcat and feedingcat includes the loss types (post harvest etc.)
* Thomas' comment: select (1) loss type = PH and Crop(AllProd) and (2) loss type = PP, D, C and ProcOutH(AllProd)
* Perhaps a set FoodWaste(LossType,AllProd) would make the formulation more concise here and at other locations...
* There is no animal nutrition data for crops, so PHLoss cannot be used as feed (i.e., vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,PHLoss,Crop)=0)!
*equation eFoodWasteAvailable(C,LossType,AllProd) "Available food waste to be used as fertilizer and/or feed" ;
*         eFoodWasteAvailable(C,LossType,AllProd)$((PHLoss(LossType) and Crop(AllProd)) or (ord(LossType)>1 and ord(LossType)<card(LossType) and ProcOutH(AllProd)))..
*         vFoodWasteAv(C,LossType,AllProd) * 1000 =e=
*                 sum(Crop$CropInC(C,Crop), vFertFoodWastekg(C,Crop,LossType,AllProd)) +
*                 sum(APSLevelHerdType, vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,LossType,AllProd)) /  (1 - LossFraction('Foodwaste',AllProd,C,FLoss))
*;
equation eWasteAvailableSum(C,LossType) "Available food waste for summed loss types given in WasteSum (DLoss and CLoss)" ;
         eWasteAvailableSum(C,WasteSum)..
         sum(ProcOutH, vFoodWasteAv(C,WasteSum,ProcOutH)) * 1000 =e=
            sum(Crop$CropInC(C,Crop), vFertFoodWasteSumkg(C,Crop,WasteSum)) +
            sum(APSLevelHerdType, vFoodWasteIntakeHerdTypeSumkgDM(C,APSLevelHerdType,WasteSum)) * 1000 / AniNutrByProductC(C,WasteSum,'DM') / (1 - FeedingLossFraction(C))
;

* NOTE: - in contrast to Ben's NL model, PH waste is no feed because there is no data on it (Animal_Nutr)
*       - we currently cannot use PH loss of MeatMilkEggs and Fish for anything (no feed nutrition data, no fert data)
* What will happen:
* - Fert: MME, Fish 'HarvestWaste' fertilisation data will be added to sheet FRV - DONE
* - Animal nutrients: 'HarvestWaste' (Column C) 'Product' (Column E) and nutr data in sheet Animal_Nutr - DONE
* Still missing: apply / (1 - LossFraction('HarvestWaste',AllProd,C,FLoss)) to vFoodWasteIntakeHerdTypekgDM, add HarvestWaste to Loss_Fraction_New
* $AniNutrByProduct('HarvestWaste',AllProd,'DM') necessary beacuse of missing DM data...
equation eWasteAvailablePH(C,AllProd) "Available postharvest waste to be used as fertilizer" ;
         eWasteAvailablePH(C,AllProd)$ProdPHLoss(C,AllProd).. // $sum(ProdCat, LossFraction(ProdCat,AllProd,C,PHLoss)).. //$LossFraction('Crop',AllProd,C,PHLoss)..
         vFoodWasteAv(C,PHLoss,AllProd) * 1000 =e=
            sum(Crop$CropInC(C,Crop), vFertFoodWastekg(C,Crop,PHLoss,AllProd)) +
            (sum(APSLevelHerdType, vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,PHLoss,AllProd))
                * 1000 / AniNutrByProduct('HarvestWaste',AllProd,'DM'))$AniNutrByProduct('HarvestWaste',AllProd,'DM')
;

* $AniNutrByProduct('Foodwaste',ProcOutH,'DM') necessary beacuse of missing DM data...
equation eWasteAvailablePP(C,LossType,AllProd) "Available food waste for 'individual' loss types given in WasteByProd (PH and PP) but EXCEPT PHLoss to be used as fertilizer and/or feed" ;
         eWasteAvailablePP(C,WasteByProd,ProcOutH)$(not PHLoss(WasteByProd))..
         vFoodWasteAv(C,WasteByProd,ProcOutH) * 1000 =e=
            sum(Crop$CropInC(C,Crop), vFertFoodWastekg(C,Crop,WasteByProd,ProcOutH)) +
            (sum(APSLevelHerdType, vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,WasteByProd,ProcOutH)) * 1000 / AniNutrByProduct('Foodwaste',ProcOutH,'DM') / (1 - LossFraction('Foodwaste',ProcOutH,C,FLoss)))$AniNutrByProduct('Foodwaste',ProcOutH,'DM')
;
$iftheni "%animalSourcedFeed%" == "off"
vFoodWasteIntakeHerdTypekgDM.fx(C,APSLevelHerdType,WasteByProd,ProcOutH)$sum(AniSourcedFeed, AniProdFamProd(AniSourcedFeed,ProcOutCP)) = 0;
$endif

* Don't forget to include PHLoss once feeding loss is in Loss_Fraction_New!
* Remove $AniNutrByProduct('Co-Product',ProcOutCP,'DM'), $AniNutrByProduct('Foodwaste',ProcOutH,'DM'), and $AniNutrByProductC(C,WasteSum,'DM') once the data is complete!
equation eFeedingWasteAvailable(C) "Feeding losses of foodwaste, co-products, and grass available as fertilizer" ;
         eFeedingWasteAvailable(C)..
            sum((APSLevelHerdType,WasteSum)$AniNutrByProductC(C,WasteSum,'DM'), vFoodWasteIntakeHerdTypeSumkgDM(C,APSLevelHerdType,WasteSum) * FeedingLossFraction(C)  * 1000 / AniNutrByProductC(C,WasteSum,'DM') ) +
            sum((APSLevelHerdType,WasteByProd,ProcOutH)$(not PHLoss(WasteByProd) and AniNutrByProduct('Foodwaste',ProcOutH,'DM')), vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,WasteByProd,ProcOutH)  * LossFraction('Foodwaste', ProcOutH, C,FLoss)  * 1000 / AniNutrByProduct('Foodwaste',ProcOutH,'DM')) +
            sum((APSLevelHerdType,ProcOutCP)$AniNutrByProduct('Co-Product',ProcOutCP,'DM'), vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)      * LossFraction('Co-Product',ProcOutCP,C,FLoss) * 1000 / AniNutrByProduct('Co-Product',ProcOutCP,'DM') ) +
            sum((APSLevelHerdType,ProcOutG), vGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG)         * LossFraction('Grass',     ProcOutG, C,FLoss) )
*            sum((APSLevelHerdType,LossType)$(PPLoss(LossType) or DLoss(LossType) or CLoss(LossType)),
*                   vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,LossType,AllProd)       * LossFraction('Foodwaste', AllProd,C,FLoss))$ProcOutH(AllProd)    +
*            ( sum(APSLevelHerdType, vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,AllProd)) * LossFraction('Co-Product',AllProd,C,FLoss))$ProcOutCP(AllProd)        +
*            ( sum(APSLevelHerdType, vGrassIntakeHerdTypekgDM (C,APSLevelHerdType,AllProd)) * LossFraction('Grass',     AllProd,C,FLoss))$ProcOutG(AllProd)
            =e=
            sum(Crop$CropInC(C,Crop), vFertFeedingLosskg(C,Crop))
;

equation eManureAvailable(C,APS,FertNutr) "Available manure non-grazing available as fertilizer";
         eManureAvailable(C,APS,FertNutr)..
            sum(APSLevelHerdType(APS,APSLevel,HerdType), vManureAv(C,APSLevelHerdType,FertNutr)) * (1 - GrazingProportion(APS)$Rumi(APS))
            =e=
            sum(Crop$CropInC(C,Crop), vFertManure(C,Crop,APS,FertNutr))
;

equation eManureGrazingAvailable(C,Rumi,FertNutr) "Available manure grazing available as fertilizer for grass by country";
         eManureGrazingAvailable(C,Rumi,FertNutr)..
            sum(APSLevelHerdType(Rumi,APSLevel,HerdType), vManureAv(C,Rumi,APSLevel,HerdType,FertNutr) * (0 + GrazingProportion(Rumi)))
            =e=
            sum(CropGrass$CropInC(C,CropGrass), vFertGrazing(C,CropGrass,Rumi,FertNutr))
;

****** Human Diet Equations ******

* scale min of B12 to 60%
HumNutrReq('Min','B12') = 0.6 * HumNutrReq('Min','B12');

set ActHumNutr(HumNutr) / Energy, Protein, Carbohydrates, Fat_total,
                          LA, ALA, DHA, EPA,
                          Cholestrol, Na, K, P, Mg, Fe, Cu, A, Fiber, 
                          B1, B3, B6, B9, C, E, VitK,
                          Ca, Zn, B2,
                          B12
                          /
;
* nutrients excluded: Se, D, I
* B12: ok at 60% of min 

equation eHumNutr(MinMax,C,HumNutr) "Meet minimum/maximum human nutrition requirements (given in nutrient units per 100g per day)" ;
         eHumNutr(MinMax,C,HumNutr)$(ActHumNutr(HumNutr)).. 
         sum(ProcOutH, vConQtyH(C,ProcOutH) * HumNutrByProduct(ProcOutH,HumNutr)) / 365 * Power(-1,ord(MinMax))
         =l=
         1e-4*Population(C)*HumNutrReq(MinMax,HumNutr) * Power(-1,ord(MinMax))
;

equation eHumIntake(MinMax,C,ProdFam) "Human diet minimum/maximum intake values (given in grams per day)" ;
         eHumIntake(MinMax,C,ProdFam)$(ord(MinMax)=2 or (ord(MinMax)=1 and HumDiet(MinMax,ProdFam)))..
         sum(ProcOutH$FP(ProdFam,ProcOutH), vConQtyH(C,ProcOutH)) * Power(-1,ord(MinMax))
         =l=
         Population(C) * 365e-6 * HumDiet(MinMax,ProdFam) * Power(-1,ord(MinMax))
;

** Diet preference - INFEASIBLE???
*vConQtyH.fx(C,ProcOutH)$(not sum(DietProd$ProdDietMap(ProcOutH,DietProd), DietPreference(DietProd,C))) = 0;
*parameter pref(AllProd,C);
*pref(ProcOutH,C) = (not sum(DietProd$ProdDietMap(ProcOutH,DietProd), DietPreference(DietProd,C)));

****** Animal Diet Equations ******

equation eAniNutrPEReq(C,APS,APSLevel,HerdType,AniNutr) "Animal protein and energy requirements met" ;
         eAniNutrPEReq(C,APSLevelHerdType,AniPE)$AniNutrReq(APSLevelHerdType,C,AniPE)..
         sum(ProcOutCP,              vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)               * AniNutrByProduct('Co-Product',ProcOutCP,AniPE)) +
         sum(ProcOutG,               vGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG)                 * AniNutrGrass(C,ProcOutG,AniPE))       +
         sum(WasteSum,               vFoodWasteIntakeHerdTypeSumkgDM(C,APSLevelHerdType,WasteSum)          * AniNutrByProductC(C,WasteSum,AniPE))            +
         sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)), vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,WasteByProd,ProcOutH) * AniNutrByProduct('Foodwaste',ProcOutH,AniPE)) +
         sum(AllProd$ProdPHLoss(C,AllProd),                    vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,PHLoss,AllProd)       * AniNutrByProduct('HarvestWaste',AllProd,AniPE))
         =g=
         AniNutrReq(APSLevelHerdType,C,AniPE) * vNumHerdType(C,APSLevelHerdType)
;

* Note that we assume DM=1 for grass
equation eAniNutrFICReq(C,APS,APSLevel,HerdType) "Monogastric intake less then feed intake capacity" ; // consider removing AniNutrByProduct(...,'DM') dollar conditions when data is clean (CaptFish products)!
         eAniNutrFICReq(C,APSLevelHerdType(Mono,APSLevel,HerdType))..
         sum(ProcOutCP, vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)               * 1000 / AniNutrByProduct('Co-Product',ProcOutCP,'DM')) +
         sum(ProcOutG,  vGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG)) +
         sum(WasteSum,  vFoodWasteIntakeHerdTypeSumkgDM(C,APSLevelHerdType,WasteSum)          * 1000 / AniNutrByProductC(C,WasteSum,'DM'))       +
         sum((WasteByProd,ProcOutH)$(AniNutrByProduct('Foodwaste',ProcOutH,'DM') and not PHLoss(WasteByProd)),
                        vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,WasteByProd,ProcOutH) * 1000 / AniNutrByProduct('Foodwaste',ProcOutH,'DM')) +
         sum(AllProd$(AniNutrByProduct('HarvestWaste',AllProd,'DM') and ProdPHLoss(C,AllProd)),
                        vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,PHLoss,AllProd)       * 1000 / AniNutrByProduct('HarvestWaste',AllProd,'DM'))
         =l=
         AniNutrReq(APSLevelHerdType,C,'FIC') * vNumHerdType(C,APSLevelHerdType)
;
* Note that we assume DM=1 for grass
equation eMonoFeedElim(C,APS,APSLevel,HerdType) "Eliminate monogastrics feed that does not provide DM"; // might not be needed any more when DM values are complete (CaptFish products)
         eMonoFeedElim(C,APSLevelHerdType(Mono,APSLevel,HerdType))..
         sum(ProcOutCP$(not AniNutrByProduct('Co-Product',ProcOutCP,'DM')), vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)) +
         sum(WasteSum$(not AniNutrByProductC(C,WasteSum,'DM')),             vFoodWasteIntakeHerdTypeSumkgDM(C,APSLevelHerdType,WasteSum)) +
         sum((WasteByProd,ProcOutH)$(not AniNutrByProduct('Foodwaste',ProcOutH,'DM') and not PHLoss(WasteByProd)),
                                                                            vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,WasteByProd,ProcOutH)) +
         sum(AllProd$((not AniNutrByProduct('HarvestWaste',AllProd,'DM')) and ProdPHLoss(C,AllProd)), vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,PHLoss,AllProd))
         =e= 0
;

equation eAniNutrVWReq(C,APS,APSLevel,HerdType) "Ruminant intake is less then VW req" ;
         eAniNutrVWReq(C,APSLevelHerdType(Rumi,APSLevel,HerdType))..
         sum(ProcOutCP, vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)                * AniNutrByProduct('Co-Product',ProcOutCP,'VW')) +
         sum(ProcOutG,  vGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG)                  * AniNutrGrass(C,ProcOutG,'VW'))       +
         sum(WasteSum,  vFoodWasteIntakeHerdTypeSumkgDM(C,APSLevelHerdType,WasteSum)           * AniNutrByProductC(C,WasteSum,'VW')) +
         sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),
                        vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,WasteByProd,ProcOutH)  * AniNutrByProduct('Foodwaste',ProcOutH,'VW')) +
         sum(AllProd$ProdPHLoss(C,AllProd),
                        vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,PHLoss,AllProd)        * AniNutrByProduct('HarvestWaste',AllProd,'VW'))
         =l=
         AniNutrReq(APSLevelHerdType,C,'VW') * vNumHerdType(C,APSLevelHerdType)
;
equation eRumiFeedElim(C,APS,APSLevel,HerdType) "Eliminate ruminants feed that does not provide VW"; // might not be needed any more when VW values are complete?
         eRumiFeedElim(C,APSLevelHerdType(Rumi,APSLevel,HerdType))..
         sum(ProcOutCP$(not AniNutrByProduct('Co-Product',ProcOutCP,'VW')), vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)) +
         sum(ProcOutG$(not AniNutrGrass(C,ProcOutG,'VW')),                  vGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG)) +
         sum(WasteSum$(not AniNutrByProductC(C,WasteSum,'VW')),             vFoodWasteIntakeHerdTypeSumkgDM(C,APSLevelHerdType,WasteSum)) +
         sum((WasteByProd,ProcOutH)$(not AniNutrByProduct('Foodwaste',ProcOutH,'VW') and not PHLoss(WasteByProd)),
                                                                            vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,WasteByProd,ProcOutH)) +
         sum(AllProd$ProdPHLoss(C,AllProd),                                 vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,PHLoss,AllProd))
         =e= 0
;

equation eAniNutrSWReq(C,APS,APSLevel,HerdType) "Ruminant intake is greater then SW req" ;
         eAniNutrSWReq(C,APSLevelHerdType)$AniNutrReq(APSLevelHerdType,C,'SW')..
         sum(ProcOutCP,vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)      * ( AniNutrByProduct('Co-Product',ProcOutCP,'SW')-AniNutrReq(APSLevelHerdType,C,'SW')) ) +
         sum(ProcOutG, vGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG)        * ( AniNutrGrass(C,ProcOutG,'SW')                -AniNutrReq(APSLevelHerdType,C,'SW')) ) +
         sum(WasteSum, vFoodWasteIntakeHerdTypeSumkgDM(C,APSLevelHerdType,WasteSum) * ( AniNutrByProductC(C,WasteSum,'SW')           -AniNutrReq(APSLevelHerdType,C,'SW')) ) +
         sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),
                       vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,WasteByProd,ProcOutH) * ( AniNutrByProduct('Foodwaste',ProcOutH,'SW')  -AniNutrReq(APSLevelHerdType,C,'SW')) ) +
         sum(AllProd$ProdPHLoss(C,AllProd),
                       vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,PHLoss,AllProd)       * ( AniNutrByProduct('HarvestWaste',AllProd,'SW')-AniNutrReq(APSLevelHerdType,C,'SW')) )
         =g= 0

*         sum(ProcOutCP,vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)        * AniNutrByProduct('Co-Product',ProcOutCP,'SW')) +
*         sum(ProcOutG,vGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG)           * AniNutrByProduct('Grass',ProcOutG,'SW'))       +
*         sum((LossType,AllProd)$((PHLoss(LossType) and Crop(AllProd)) or (ord(LossType)>1 and ord(LossType)<card(LossType) and ProcOutH(AllProd))),
*             vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,LossType,AllProd)        * AniNutrByProduct(LossType,AllProd,'SW'))
*         =g=
*         (sum(ProcOutCP,vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP))      +
*          sum(ProcOutG,vGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG))         +
*          sum((LossType,AllProd)$((PHLoss(LossType) and Crop(AllProd)) or (ord(LossType)>1 and ord(LossType)<card(LossType) and ProcOutH(AllProd))),
*              vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,LossType,AllProd))
*         ) * AniNutrReq(APSLevelHerdType,C,'SW')
;

equation eGrazingProportion(C,Rumi,APSLevel,HerdType) "Proportion of feed intake from fresh grass" ;
         eGrazingProportion(C,APSLevelHerdType(Rumi,APSLevel,HerdType))..
         sum(FreshGrass, vGrassIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,FreshGrass) * AniNutrGrass(C,FreshGrass,'DVE')) =e=
                 AniNutrReq(Rumi,APSLevel,HerdType,C,'DVE') * vNumHerdType(C,Rumi,APSLevel,HerdType) * GrazingProportion(Rumi)
;

* eGrazingMax(C): the actually used grazing area cannot be greater than MaxHaPerAnimal(...) times the number of animals
* LHS: Maximum grazing area in the country, broken down to C,Z,Soil,LandType,CropGrass.
* RHS: This is the maximum grazing land in ha for each country due to MaxHaPerAnimal.
equation eGrazingMax(C) "Caculate maximum grazing area per country" ;
         eGrazingMax(C)..
         sum((Z,Soil,LandType,CropGrass)$(CZSL(C,Z,Soil,LandType) and CropYieldha(LandType,Soil,Z,CropGrass,C)), vHaGrazing(C,Z,Soil,LandType,CropGrass))
         =l=
         sum(APSLevelHerdType(Rumi,APSLevel,HerdType), vNumHerdType(C,Rumi,APSLevel,HerdType)*MaxHaPerAnimal(Rumi,APSLevel,HerdType,C))

* Now we use vHaGrazing to restrict the use of fresh grass (note that implicitely quite some averaging is done).
equation eGrazingArea(C) "Limit the fresh grass production in each country to the maximum allowed area vHaGrazing" ;
         eGrazingArea(C)..
         sum(FreshGrass,
            sum((Z,Soil,LandType,CropGrass)$(CZSL(C,Z,Soil,LandType) and CropYieldha(LandType,Soil,Z,CropGrass,C)),
                vHaGrazing(C,Z,Soil,LandType,CropGrass) * CropYieldha(LandType,Soil,Z,CropGrass,C) // this is the max crop grass yield
                * Processing(CropGrass,FreshGrass)) * (1-LossFraction('Crop',FreshGrass,C,PHLoss)) // this is the max individual FreshGrass product available
         ) // this sum is the max total fresh grass available
         =g=
         sum(FreshGrass, vQtyProcOut(C,FreshGrass)) // this is the actual total fresh grass available

****** Animal products availability equations ******

equation eNumHerdType(C,APS,APSLevel,HerdType) "Calculate herd types per production unit" ;
         eNumHerdType(C,APSLevelHerdType(APS,APSLevel,HerdType))$(not Producer(APS,HerdType))..
         vNumHerdType(C,APSLevelHerdType) =e= sum(HerdType1$Producer(APS,HerdType1),vNumHerdType(C,APS,APSLevel,HerdType1)) * HerdTypePerPU(APS,APSLevel,HerdType,C)
;

equation eAnimalAvailable(C,MeatMilkEggs) "Amount of animal products available" ;
         eAnimalAvailable(C,MeatMilkEggs)..
         vAnimalAv(C,MeatMilkEggs) =e= sum((APSLevelHerdType), vNumHerdType(C,APSLevelHerdType) * sum(ConC(Con,C), AniYield(APSLevelHerdType,Con,MeatMilkEggs)))
;

****** Excreta ******

equation eEx(C,APS,APSLevel,HerdType,FertNutr) "Fert excretion from animals" ;
         eEx(C,APSLevelHerdType,FertNutr)..
         vManureAv(C,APSLevelHerdType,FertNutr) =e=
             (sum(ProcOutCP,vCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)                             * AniNutrByProduct('Co-Product',ProcOutCP,FertNutr)) +
              sum(ProcOutG, vGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG)                               * AniNutrGrass(C,ProcOutG,FertNutr)) +
              sum(WasteSum, vFoodWasteIntakeHerdTypeSumkgDM(C,APSLevelHerdType,WasteSum)                        * AniNutrByProductC(C,WasteSum,FertNutr)) +
              sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)), vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,WasteByProd,ProcOutH) * AniNutrByProduct('Foodwaste',ProcOutH,FertNutr)) +
              sum(AllProd$ProdPHLoss(C,AllProd),                    vFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,PHLoss,AllProd)       * AniNutrByProduct('HarvestWaste',AllProd,FertNutr))
             )/1000 - sum(ConC(Con,C), AniNPKRet(APSLevelHerdType,Con,FertNutr)) * vNumHerdType(C,APSLevelHerdType)
;

equation eManureM(C,APS,APSLevel,HerdType,FertNutr) "Manure available after N losses in Manure Management ";
         eManureM(C,APSLevelHerdType(APS,APSLevel,HerdType),FertNutr)..
         vManureAv(C,APSLevelHerdType,FertNutr) * (1 - GrazingProportion(APS)) - vNLossesMM(C,APSLevelHerdType,FertNutr)$diag(FertNutr,'N')
         =e=
         vManureM(C,APSLevelHerdType,FertNutr)
;

equation eExH(C,FertNutr) "Fert excretion from humans" ;
         eExH(C,FertNutr)..
         vHExAv(C,FertNutr) =e= sum(ProcOutH,vConQtyH(C,ProcOutH) * sum(HumNutr$diag(HumNutr,FertNutr),HumNutrByProduct(ProcOutH,HumNutr))) * 1e4
                                * (1 - sum(Con$ConC(Con,C),HumNutrLost(Con))) - Population(C)*Human_NPK(FertNutr)
;

equation eExHAv(C,FertNutr) "Human excreta available" ;
         eExHAv(C,FertNutr)..
         vHExAv(C,FertNutr) =g= sum(Crop$CropInC(C,Crop),vFertH(C,Crop,FertNutr))
;

****** GHG emissions ******

** Transportation emissions **

$iftheni.anyInOut "%anyInOut%"  == "on"
equation eCO2Transport(C) "CO2 from transportation, assigned to country of origin" ;
         eCO2Transport(C)..
         vCO2Transport(C)*0.001 =e=
$ifi "%procInOut%" == "on"
            sum((C1,ProcOut)$(not diag(C,C1) and (ProcOutH(ProcOut) or ProcOutCP(ProcOut))), vFromTo(C,C1,ProcOut)  * EFTransport('kg CO2') / %ShipmentSize% * Distance(C,C1)/1000)
$ifi "%cropInOut%"  == "on"
            +sum((C1,Crop)$(not diag(C,C1) and not CropGrass(Crop)), vFromToCrop(C,C1,Crop) * EFTransport('kg CO2') / %ShipmentSize% * Distance(C,C1)/1000)
;
equation eCH4Transport(C) "CH4 from transportation, assigned to country of origin" ;
         eCH4Transport(C)..
         vCH4Transport(C)*0.001 =e=
$ifi "%procInOut%" == "on"
            sum((C1,ProcOut)$(not diag(C,C1) and (ProcOutH(ProcOut) or ProcOutCP(ProcOut))), vFromTo(C,C1,ProcOut)  * EFTransport('kg CH4') / %ShipmentSize% * Distance(C,C1)/1000)
$ifi "%cropInOut%"  == "on"
            +sum((C1,Crop)$(not diag(C,C1) and not CropGrass(Crop)), vFromToCrop(C,C1,Crop) * EFTransport('kg CH4') / %ShipmentSize% * Distance(C,C1)/1000)
;
equation eN2OTransport(C) "N2O from transportation, assigned to country of origin" ;
         eN2OTransport(C)..
         vN2OTransport(C)*0.001 =e=
$ifi "%procInOut%" == "on"
         sum((C1,ProcOut)$(not diag(C,C1) and (ProcOutH(ProcOut) or ProcOutCP(ProcOut))), vFromTo(C,C1,ProcOut)  * EFTransport('kg N2O') / %ShipmentSize% * Distance(C,C1)/1000)
$ifi "%cropInOut%"  == "on"
         +sum((C1,Crop)$(not diag(C,C1) and not CropGrass(Crop)), vFromToCrop(C,C1,Crop) * EFTransport('kg N2O') / %ShipmentSize% * Distance(C,C1)/1000)
;
$else.anyInOut
vCO2Transport.fx(C) = 0;
vCH4Transport.fx(C) = 0;
vN2OTransport.fx(C) = 0;
$endif.anyInOut

** Land use change emissions **

equation eCO2eqLUC(C) "CO2 from and use change" ;
         eCO2eqLUC(C)..
         vCO2eqLUC(C) =e= LUC('Pasture','crop') * sum((Z,Crop)$(CropYieldha('Arable','pasture',Z,Crop,C) and not CropGrass(Crop)), vNumha(C,Z,'pasture','Arable',Crop)) +
                          LUC('crop','Pasture') * sum((Z,CropGrass)$CropYieldha('Arable','non-peat',Z,CropGrass,C), vNumha(C,Z,'non-peat','Arable',CropGrass))
;

** TAN equations **

equation eTANPig(C,APS,APSLevel,HerdType) "TAN excretion from Pigs";
         eTANPig(C,'Pig',APSLevel,HerdType)$APSLevelHerdType('Pig',APSLevel,HerdType)..
         vTANExcretionMM(C,'Pig',APSLevel,HerdType) =e=
             ((((sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,'Pig',APSLevel,HerdType,ProcOutCP)                 * AniNutrByProduct('Co-Product',ProcOutCP,'CP') * AniNutrByProduct('Co-Product',ProcOutCP,'DCCPP')  * 0.01))  +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,'Pig',APSLevel,HerdType,WasteByProd,ProcOutH)   * AniNutrByProduct('Foodwaste',ProcOutH,'CP')   * AniNutrByProduct('Foodwaste',ProcOutH,'DCCPP')    * 0.01))  +
                 sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,'Pig',APSLevel,HerdType,PHLoss,AllProd)         * AniNutrByProduct('HarvestWaste',AllProd,'CP') * AniNutrByProduct('HarvestWaste',AllProd,'DCCPP')  * 0.01))  +
                 sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSumkgDM(C,'Pig',APSLevel,HerdType,WasteSum)            * AniNutrByProductC(C,WasteSum,'CP')            * AniNutrByProductC(C,WasteSum,'DCCPP')             * 0.01))
                ) * 0.16) - sum(ConC(Con,C), AniNPKRet('Pig',APSLevel,HerdType,Con,'N')) * vNumHerdType(C,'Pig',APSLevel,HerdType) * 1000) * 28/60) * 0.001
;
equation eTANRumiMM(C,APS,APSLevel,HerdType) "TAN excretion from Ruminant housing";
         eTANRumiMM(C,Rumi,APSLevel,HerdType)$APSLevelHerdType(Rumi,APSLevel,HerdType)..
         vTANExcretionMM(C,Rumi,APSLevel,HerdType) =e=
             ((((sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,ProcOutCP)                  * AniNutrByProduct('Co-Product',ProcOutCP,'CP') * AniNutrByProduct('Co-Product',ProcOutCP,'DCCPR')  * 0.01))  +
                 sum(ProcOutG,                                          (vGrassIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,ProcOutG)                    * AniNutrGrass(C,ProcOutG,'CP')                 * AniNutrGrass(C,ProcOutG,'DCCPR')                  * 0.01))  +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,WasteByProd,ProcOutH)    * AniNutrByProduct('Foodwaste',ProcOutH,'CP')   * AniNutrByProduct('Foodwaste',ProcOutH,'DCCPR')    * 0.01))  +
                 sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,PHLoss,AllProd)          * AniNutrByProduct('HarvestWaste',AllProd,'CP') * AniNutrByProduct('HarvestWaste',AllProd,'DCCPR')  * 0.01))  +
                 sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSumkgDM(C,Rumi,APSLevel,HerdType,WasteSum)             * AniNutrByProductC(C,WasteSum,'CP')            * AniNutrByProductC(C,WasteSum,'DCCPR')             * 0.01))
                ) * 0.16) - sum(ConC(Con,C), AniNPKRet(Rumi,APSLevel,HerdType,Con,'N')) * vNumHerdType(C,Rumi,APSLevel,HerdType) * 1000) * 28/60) * 0.001 * (1 - GrazingProportion(Rumi))
;       
equation eTANRumiG(C,APS,APSLevel,HerdType) "TAN excretion from Ruminant Grazing";
         eTANRumiG(C,Rumi,APSLevel,HerdType)$APSLevelHerdType(Rumi,APSLevel,HerdType)..
         vTANExcretionG(C,Rumi,APSLevel,HerdType) =e=
             ((((sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,ProcOutCP)                  * AniNutrByProduct('Co-Product',ProcOutCP,'CP') * AniNutrByProduct('Co-Product',ProcOutCP,'DCCPR')  * 0.01))  +
                 sum(ProcOutG,                                          (vGrassIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,ProcOutG)                    * AniNutrGrass(C,ProcOutG,'CP')                 * AniNutrGrass(C,ProcOutG,'DCCPR')                  * 0.01))  +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,WasteByProd,ProcOutH)    * AniNutrByProduct('Foodwaste',ProcOutH,'CP')   * AniNutrByProduct('Foodwaste',ProcOutH,'DCCPR')    * 0.01))  +
                 sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,PHLoss,AllProd)          * AniNutrByProduct('HarvestWaste',AllProd,'CP') * AniNutrByProduct('HarvestWaste',AllProd,'DCCPR')  * 0.01))  +
                 sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSumkgDM(C,Rumi,APSLevel,HerdType,WasteSum)             * AniNutrByProductC(C,WasteSum,'CP')            * AniNutrByProductC(C,WasteSum,'DCCPR')             * 0.01))
                ) * 0.16) - sum(ConC(Con,C), AniNPKRet(Rumi,APSLevel,HerdType,Con,'N')) * vNumHerdType(C,Rumi,APSLevel,HerdType) * 1000) * 28/60) * 0.001 * GrazingProportion(Rumi)
;       
equation eTANLayer(C,APS,APSLevel,HerdType) "TAN excretion from layers";
         eTANLayer(C,'Layer',APSLevel,HerdType)$APSLevelHerdType('Layer',APSLevel,HerdType)..
         vTANExcretionMM(C,'Layer',APSLevel,HerdType) =e=
             ((((sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,'Layer',APSLevel,HerdType,ProcOutCP)               * AniNutrByProduct('Co-Product',ProcOutCP,'CP') * AniNutrByProduct('Co-Product',ProcOutCP,'DCCPL')  * 0.01))  +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,'Layer',APSLevel,HerdType,WasteByProd,ProcOutH) * AniNutrByProduct('Foodwaste',ProcOutH,'CP')   * AniNutrByProduct('Foodwaste',ProcOutH,'DCCPL')    * 0.01))  +
                 sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,'Layer',APSLevel,HerdType,PHLoss,AllProd)       * AniNutrByProduct('HarvestWaste',AllProd,'CP') * AniNutrByProduct('HarvestWaste',AllProd,'DCCPL')  * 0.01))  +
                 sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSumkgDM(C,'Layer',APSLevel,HerdType,WasteSum)          * AniNutrByProductC(C,WasteSum,'CP')            * AniNutrByProductC(C,WasteSum,'DCCPL')             * 0.01))
                ) * 0.16) - sum(ConC(Con,C), AniNPKRet('Layer',APSLevel,HerdType,Con,'N')) * vNumHerdType(C,'Layer',APSLevel,HerdType) * 1000) * 56/168) * 0.001
;       
equation eTANBroiler(C,APS,APSLevel,HerdType) "TAN excretion from Broiler";
         eTANBroiler(C,'Broiler',APSLevel,HerdType)$APSLevelHerdType('Broiler',APSLevel,HerdType)..
         vTANExcretionMM(C,'Broiler',APSLevel,HerdType) =e=
             ((((sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,'Broiler',APSLevel,HerdType,ProcOutCP)             * AniNutrByProduct('Co-Product',ProcOutCP,'CP') * AniNutrByProduct('Co-Product',ProcOutCP,'DCCPBr') * 0.01))  +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,'Broiler',APSLevel,HerdType,WasteByProd,ProcOutH)*AniNutrByProduct('Foodwaste',ProcOutH,'CP')   * AniNutrByProduct('Foodwaste',ProcOutH,'DCCPBr')   * 0.01))  +
                 sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,'Broiler',APSLevel,HerdType,PHLoss,AllProd)     * AniNutrByProduct('HarvestWaste',AllProd,'CP') * AniNutrByProduct('HarvestWaste',AllProd,'DCCPBr') * 0.01))  +
                 sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSumkgDM(C,'Broiler',APSLevel,HerdType,WasteSum)        * AniNutrByProductC(C,WasteSum,'CP')            * AniNutrByProductC(C,WasteSum,'DCCPBr')            * 0.01))
                ) * 0.16) - sum(ConC(Con,C), AniNPKRet('Broiler',APSLevel,HerdType,Con,'N')) * vNumHerdType(C,'Broiler',APSLevel,HerdType) * 1000) * 56/168) * 0.001
;

** Manure management **

equation eN2ONDMM(C,APS,APSLevel,HerdType) "Direct N2O-N emissions from manure management";
         eN2ONDMM(C,APS,APSLevel,HerdType)$(APSLevelHerdType(APS,APSLevel,HerdType) and (not sameas('Tilapia',APS)) and (not sameas('Salmon',APS)))..
         vN2ONDMM(C,APS,APSLevel,HerdType) =e= vManureM(C,APS,APSLevel,HerdType,'N') * EFH(C,APS,'N2ODH')
;       
equation eNOXNMM(C,APS,APSLevel,HerdType) "NOx-N emissions from manure management";
         eNOXNMM(C,APS,APSLevel,HerdType)$(APSLevelHerdType(APS,APSLevel,HerdType) and (not sameas('Tilapia',APS)) and (not sameas('Salmon',APS)))..
         vNOXNMM(C,APS,APSLevel,HerdType) =e= vManureM(C,APS,APSLevel,HerdType,'N') * EFH(C,APS,'NOXH')
;       
equation eNH3NMM(C,APS,APSLevel,HerdType) "NH3-N emissions from manure management";
         eNH3NMM(C,APS,APSLevel,HerdType)$(APSLevelHerdType(APS,APSLevel,HerdType) and (not sameas('Tilapia',APS)) and (not sameas('Salmon',APS)))..
         vNH3NMM(C,APS,APSLevel,HerdType) =e=  vTANExcretionMM(C,APS,APSLevel,HerdType) * EFH(C,APS,'NH3H')
;       
equation eN2NMM(C,APS,APSLevel,HerdType) "NH3-N emissions from manure management";
         eN2NMM(C,APS,APSLevel,HerdType)$(APSLevelHerdType(APS,APSLevel,HerdType) and (not sameas('Tilapia',APS)) and (not sameas('Salmon',APS)))..
         vN2NMM(C,APS,APSLevel,HerdType) =e=  vManureM(C,APS,APSLevel,HerdType,'N') * EFH(C,APS,'N2H')
;       
equation eN2ONIDMM(C,APS,APSLevel,HerdType) "InDirect N2O-N emissions from manure management";
         eN2ONIDMM(C,APS,APSLevel,HerdType)$(APSLevelHerdType(APS,APSLevel,HerdType) and (not sameas('Tilapia',APS)) and (not sameas('Salmon',APS)))..
         vN2ONIDMM(C,APS,APSLevel,HerdType) =e=
                 (vNOXNMM(C,APS,APSLevel,HerdType) * 14/30) + (vNH3NMM(C,APS,APSLevel,HerdType) * 17/14) * EFH(C,APS,'N2OIDH')
;       
equation eN2ONFishM(C,Aquatic,APSLevel,HerdType) "N2O emissions from aquaciulture";
         eN2ONFishM(C,Aquatic,APSLevel,HerdType)$APSLevelHerdType(Aquatic,APSLevel,HerdType)..
         vN2ONDeNitrM(C,Aquatic,APSLevel,HerdType) =e= vManureAv(C,Aquatic,APSLevel,HerdType,'N') * 0.018
;       
equation eN2ONFishFeed(C,Aquatic,APSLevel,HerdType) "N2O emissions from aquaciulture";
         eN2ONFishFeed(C,Aquatic,APSLevel,HerdType)$APSLevelHerdType(Aquatic,APSLevel,HerdType)..
         vN2ONDeNitrFeed(C,Aquatic,APSLevel,HerdType) =e=
                (sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,Aquatic,APSLevel,HerdType,ProcOutCP)               * AniNutrByProduct('Co-Product',ProcOutCP,'N')  * 0.001 * LossFraction('Co-Product',ProcOutCP,C,FLoss)  ))  +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,Aquatic,APSLevel,HerdType,WasteByProd,ProcOutH) * AniNutrByProduct('Foodwaste',ProcOutH,'N')    * 0.001 * LossFraction('Foodwaste',ProcOutH,C,FLoss)))       +
                 sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,Aquatic,APSLevel,HerdType,PHLoss,AllProd)       * AniNutrByProduct('HarvestWaste',AllProd,'N')  * 0.001 * LossFraction('HarvestWaste',AllProd,C,FLoss)))  +
                 sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSumkgDM(C,Aquatic,APSLevel,HerdType,WasteSum)          * AniNutrByProductC(C,WasteSum,'N')             * 0.001 * FeedingLossFraction(C)))
                ) * 0.018
;
equation eNLossesMM(C,APS,APSLevel,HerdType) "N losses from manure management";
         eNLossesMM(C,APS,APSLevel,HerdType)$APSLevelHerdType(APS,APSLevel,HerdType)..
         vNLossesMM(C,APS,APSLevel,HerdType,'N') =e=
                    vN2ONDMM(C,APS,APSLevel,HerdType)$APSLand(APS)  + vNOXNMM(C,APS,APSLevel,HerdType)$APSLand(APS) + vNH3NMM(C,APS,APSLevel,HerdType)$APSLand(APS) +
                    vN2NMM(C,APS,APSLevel,HerdType)$APSLand(APS)    + vN2ONDeNitrM(C,APS,APSLevel,HerdType)$Aquatic(APS)
;
*vNLossesMM.FX(C,APS,APSLevel,HerdType,'P')=0;
*vNLossesMM.FX(C,APS,APSLevel,HerdType,'K')=0;

equation eVSExcretionPig(C,APS,APSLevel,HerdType) "Volatile solid excretion from Pigs";
         eVSExcretionPig(C,'Pig',APSLevel,HerdType)$APSLevelHerdType('Pig',APSLevel,HerdType)..
         vVSExcretionMM(C,'Pig',APSLevel,HerdType) =e=
                (sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,'Pig',APSLevel,HerdType,ProcOutCP)             * ((1000 - AniNutrByProduct('Co-Product',ProcOutCP,'ASH'))  - ((1000 - AniNutrByProduct('Co-Product',ProcOutCP,'ASH'))  * AniNutrByProduct('Co-Product',ProcOutCP,'DCOMP')  * 0.01)))) +
                 sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSumkgDM(C,'Pig',APSLevel,HerdType,WasteSum)        * ((1000 - AniNutrByProductC(C,WasteSum,'ASH'))             - ((1000 - AniNutrByProductC(C,WasteSum,'ASH'))             * AniNutrByProductC(C,WasteSum,'DCOMP')             * 0.01)))) +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,'Pig',APSLevel,HerdType,WasteByProd,ProcOutH)*((1000 - AniNutrByProduct('Foodwaste',ProcOutH,'ASH'))    - ((1000 - AniNutrByProduct('Foodwaste',ProcOutH,'ASH'))    * AniNutrByProduct('Foodwaste',ProcOutH,'DCOMP')    * 0.01)))) +
                 sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,'Pig',APSLevel,HerdType,PHLoss,AllProd)     * ((1000 - AniNutrByProduct('HarvestWaste',AllProd,'ASH'))  - ((1000 - AniNutrByProduct('HarvestWaste',AllProd,'ASH'))  * AniNutrByProduct('HarvestWaste',AllProd,'DCOMP')  * 0.01))))
                ) * 0.001 +
                 vTANExcretionMM(C,'Pig',APSLevel,Herdtype)
;       
equation eVSExcretionMMRumi(C,APS,APSLevel,HerdType) "Volatile solid excretion from ruminant housing";
         eVSExcretionMMRumi(C,Rumi,APSLevel,HerdType)$APSLevelHerdType(Rumi,APSLevel,HerdType)..
         vVSExcretionMM(C,Rumi,APSLevel,HerdType) =e=
                (sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,ProcOutCP)              * ((1000 - AniNutrByProduct('Co-Product',ProcOutCP,'ASH'))  - ((1000 - AniNutrByProduct('Co-Product',ProcOutCP,'ASH'))  * AniNutrByProduct('Co-Product',ProcOutCP,'DCOMR')  * 0.01)))) +
                 sum(ProcOutG,                                          (vGrassIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,ProcOutG)                * ((1000 - AniNutrGrass(C,ProcOutG,'ASH'))                  - ((1000 - AniNutrGrass(C,ProcOutG,'ASH'))                  * AniNutrGrass(C,ProcOutG,'DCOMR')                  * 0.01)))) +
                 sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSUMkgDM(C,Rumi,APSLevel,HerdType,WasteSum)         * ((1000 - AniNutrByProductC(C,WasteSum,'ASH'))             - ((1000 - AniNutrByProductC(C,WasteSum,'ASH'))             * AniNutrByProductC(C,WasteSum,'DCOMR')             * 0.01)))) +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,WasteByProd,ProcOutH)* ((1000 - AniNutrByProduct('Foodwaste',ProcOutH,'ASH'))    - ((1000 - AniNutrByProduct('Foodwaste',ProcOutH,'ASH'))    * AniNutrByProduct('Foodwaste',ProcOutH,'DCOMR')    * 0.01)))) +
                 sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,PHLoss,AllProd)      * ((1000 - AniNutrByProduct('HarvestWaste',AllProd,'ASH'))  - ((1000 - AniNutrByProduct('HarvestWaste',AllProd,'ASH'))  * AniNutrByProduct('HarvestWaste',AllProd,'DCOMR')  * 0.01))))
                ) * 0.001 * (1 - GrazingProportion(Rumi)) +
                 vTANExcretionMM(C,Rumi,APSLevel,HerdType)
;       
equation eVSExcretionGRumi(C,APS,APSLevel,HerdType) "Volatile solid excretion from ruminant grazing";
         eVSExcretionGRumi(C,Rumi,APSLevel,HerdType)$APSLevelHerdType(Rumi,APSLevel,HerdType)..
         vVSExcretionG(C,Rumi,APSLevel,HerdType) =e=
                (sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,ProcOutCP)              * ((1000 - AniNutrByProduct('Co-Product',ProcOutCP,'ASH'))  - ((1000 - AniNutrByProduct('Co-Product',ProcOutCP,'ASH'))  * AniNutrByProduct('Co-Product',ProcOutCP,'DCOMR')  * 0.01)))) +
                 sum(ProcOutG,                                          (vGrassIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,ProcOutG)                * ((1000 - AniNutrGrass(C,ProcOutG,'ASH'))                  - ((1000 - AniNutrGrass(C,ProcOutG,'ASH'))                  * AniNutrGrass(C,ProcOutG,'DCOMR')                  * 0.01)))) +
                 sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSumkgDM(C,Rumi,APSLevel,HerdType,WasteSum)         * ((1000 - AniNutrByProductC(C,WasteSum,'ASH'))             - ((1000 - AniNutrByProductC(C,WasteSum,'ASH'))             * AniNutrByProductC(C,WasteSum,'DCOMR')             * 0.01)))) +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,WasteByProd,ProcOutH)* ((1000 - AniNutrByProduct('Foodwaste',ProcOutH,'ASH'))    - ((1000 - AniNutrByProduct('Foodwaste',ProcOutH,'ASH'))    * AniNutrByProduct('Foodwaste',ProcOutH,'DCOMR')    * 0.01)))) +
                 sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,PHLoss,AllProd)      * ((1000 - AniNutrByProduct('HarvestWaste',AllProd,'ASH'))  - ((1000 - AniNutrByProduct('HarvestWaste',AllProd,'ASH'))  * AniNutrByProduct('HarvestWaste',AllProd,'DCOMR')  * 0.01))))
                ) * 0.001 * GrazingProportion(Rumi) +
                 vTANExcretionG(C,Rumi,APSLevel,HerdType)
;       
equation eVSExcretionLayer(C,APS,APSLevel,HerdType) "Volatile solid excretion from Layer";
         eVSExcretionLayer(C,'Layer',APSLevel,HerdType)$APSLevelHerdType('Layer',APSLevel,HerdType)..
         vVSExcretionMM(C,'Layer',APSLevel,HerdType) =e=
                (sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,'Layer',APSLevel,HerdType,ProcOutCP)               * ((1000 - AniNutrByProduct('Co-Product',ProcOutCP,'ASH'))  - ((AniNutrByProduct('Co-Product',ProcOutCP,'CP')   * AniNutrByProduct('Co-Product',ProcOutCP,'DCCPL')  * 0.01) + (AniNutrByProduct('Co-Product',ProcOutCP,'CFAT')  * AniNutrByProduct('Co-Product',ProcOutCP,'DCFATL') * 0.01) + (AniNutrByProduct('Co-Product',ProcOutCP,'NFE')   * AniNutrByProduct('Co-Product',ProcOutCP,'DCNFEL') * 0.01))))) +
                 sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSumkgDM(C,'Layer',APSLevel,HerdType,WasteSum)          * ((1000 - AniNutrByProductC(C,WasteSum,'ASH'))             - ((AniNutrByProductC(C,WasteSum,'CP')              * AniNutrByProductC(C,WasteSum,'DCCPL')             * 0.01) + (AniNutrByProductC(C,WasteSum,'CFAT')             * AniNutrByProductC(C,WasteSum,'DCFATL')            * 0.01) + (AniNutrByProductC(C,WasteSum,'NFE')              * AniNutrByProductC(C,WasteSum,'DCNFEL')            * 0.01))))) +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,'Layer',APSLevel,HerdType,WasteByProd,ProcOutH) * ((1000 - AniNutrByProduct('Foodwaste',ProcOutH,'ASH'))    - ((AniNutrByProduct('Foodwaste',ProcOutH,'CP')     * AniNutrByProduct('Foodwaste',ProcOutH,'DCCPL')    * 0.01) + (AniNutrByProduct('Foodwaste',ProcOutH,'CFAT')    * AniNutrByProduct('Foodwaste',ProcOutH,'DCFATL')   * 0.01) + (AniNutrByProduct('Foodwaste',ProcOutH,'NFE')     * AniNutrByProduct('Foodwaste',ProcOutH,'DCNFEL')   * 0.01))))) +
                 sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,'Layer',APSLevel,HerdType,PHLoss,AllProd)       * ((1000 - AniNutrByProduct('HarvestWaste',AllProd,'ASH'))  - ((AniNutrByProduct('HarvestWaste',AllProd,'CP')   * AniNutrByProduct('HarvestWaste',AllProd,'DCCPL')  * 0.01) + (AniNutrByProduct('HarvestWaste',AllProd,'CFAT')  * AniNutrByProduct('HarvestWaste',AllProd,'DCFATL') * 0.01) + (AniNutrByProduct('HarvestWaste',AllProd,'NFE')   * AniNutrByProduct('HarvestWaste',AllProd,'DCNFEL') * 0.01)))))
                ) * 0.001 +
                 vTANExcretionMM(C,'Layer',APSLevel,HerdType)
;       
equation eVSExcretionBroiler(C,APS,APSLevel,HerdType) "Volatile solid excretion from Layer";
         eVSExcretionBroiler(C,'Broiler',APSLevel,HerdType)$APSLevelHerdType('Broiler',APSLevel,HerdType)..
         vVSExcretionMM(C,'Broiler',APSLevel,HerdType) =e=
                (sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,'Broiler',APSLevel,HerdType,ProcOutCP)             * ((1000 - AniNutrByProduct('Co-Product',ProcOutCP,'ASH'))  - ((AniNutrByProduct('Co-Product',ProcOutCP,'CP')   * AniNutrByProduct('Co-Product',ProcOutCP,'DCCPBr') * 0.01) + (AniNutrByProduct('Co-Product',ProcOutCP,'CFAT')  * AniNutrByProduct('Co-Product',ProcOutCP,'DCFATBr')* 0.01) + (AniNutrByProduct('Co-Product',ProcOutCP,'NFE')   * AniNutrByProduct('Co-Product',ProcOutCP,'DCNFEBr')* 0.01))))) +
                 sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSumkgDM(C,'Broiler',APSLevel,HerdType,WasteSum)        * ((1000 - AniNutrByProductC(C,WasteSum,'ASH'))             - ((AniNutrByProductC(C,WasteSum,'CP')              * AniNutrByProductC(C,WasteSum,'DCCPBr')            * 0.01) + (AniNutrByProductC(C,WasteSum,'CFAT')             * AniNutrByProductC(C,WasteSum,'DCFATBr')           * 0.01) + (AniNutrByProductC(C,WasteSum,'NFE')              * AniNutrByProductC(C,WasteSum,'DCNFEBr')           * 0.01))))) +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,'Broiler',APSLevel,HerdType,WasteByProd,ProcOutH)*((1000 - AniNutrByProduct('Foodwaste',ProcOutH,'ASH'))    - ((AniNutrByProduct('Foodwaste',ProcOutH,'CP')     * AniNutrByProduct('Foodwaste',ProcOutH,'DCCPBr')   * 0.01) + (AniNutrByProduct('Foodwaste',ProcOutH,'CFAT')    * AniNutrByProduct('Foodwaste',ProcOutH,'DCFATBr')  * 0.01) + (AniNutrByProduct('Foodwaste',ProcOutH,'NFE')     * AniNutrByProduct('Foodwaste',ProcOutH,'DCNFEBr')  * 0.01))))) +
                 sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,'Broiler',APSLevel,HerdType,PHLoss,AllProd)     * ((1000 - AniNutrByProduct('HarvestWaste',AllProd,'ASH'))  - ((AniNutrByProduct('HarvestWaste',AllProd,'CP')   * AniNutrByProduct('HarvestWaste',AllProd,'DCCPBr') * 0.01) + (AniNutrByProduct('HarvestWaste',AllProd,'CFAT')  * AniNutrByProduct('HarvestWaste',AllProd,'DCFATBr')* 0.01) + (AniNutrByProduct('HarvestWaste',AllProd,'NFE')   * AniNutrByProduct('HarvestWaste',AllProd,'DCNFEBr')* 0.01)))))
                ) * 0.001 +
                 vTANExcretionMM(C,'Broiler',APSLevel,HerdType)
;       
equation eCH4Manure(C,APS,APSLevel,HerdType) "Methane emissions from manure management";
         eCH4Manure(C,APS,APSLevel,HerdType)$APSLevelHerdType(APS,APSLevel,HerdType)..
         vCH4Manure(C,APS,APSLevel,HerdType) =e=
                (vVSExcretionMM(C,APS,APSLevel,HerdType)  * EFH(C,APS,'MCFH')  * EFH(C,APS,'B0H') * 0.67) +
                (vVSExcretionG(C,APS,APSLevel,HerdType)   * EFH(C,APS,'MCFGR') * EFH(C,APS,'B0H') * 0.67)
;       
equation eCH4Enteric(C,APS,APSLevel,HerdType) "Methane emissions from enteric fermentation";
         eCH4Enteric(C,Rumi,APSLevel,HerdType)$APSLevelHerdType(Rumi,APSLevel,HerdType)..
         vCH4Enteric(C,Rumi,APSLevel,HerdType) =e=
                (sum(ProcOutCP,                                         (vCoProdIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,ProcOutCP)              * AniNutrByProduct('Co-Product',ProcOutCP,'GE') * 0.065) /55.65)) +
                (sum(ProcOutG,                                          (vGrassIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,ProcOutG)                * AniNutrGrass(C,ProcOutG,'GE')                 * 0.065) /55.65)) +
                (sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)),  (vFoodWasteIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,WasteByProd,ProcOutH)* AniNutrByProduct('Foodwaste',ProcOutH,'GE')   * 0.065) /55.65)) +
                (sum(AllProd$ProdPHLoss(C,Allprod),                     (vFoodWasteIntakeHerdTypekgDM(C,Rumi,APSLevel,HerdType,PHLoss,AllProd)      * AniNutrByProduct('HarvestWaste',AllProd,'GE') * 0.065) /55.65)) +
                (sum(WasteSum,                                          (vFoodWasteIntakeHerdTypeSumkgDM(C,Rumi,APSLevel,HerdType,WasteSum)         * AniNutrByProductC(C,WasteSum,'GE')            * 0.065) /55.65))
;

equation eCO2eqAnimal(C,APS,APSLevel) "CO2eq emissions from animals";
         eCO2eqAnimal(C,APS,APSLevel)$APSAPSLevel(APS,APSLevel)..
         vCO2eqAnimal(C,APS,APSLevel) =e= sum((HerdType)$APSLevelHerdType(APS,APSLevel,HerdType),
                 ((vCH4Manure(C,APS,APSLevel,HerdType) + vCH4Enteric(C,APS,APSLevel,HerdType)) * GHGequiv('CH4')) +
                 ((vN2ONDMM(C,APS,APSLevel,HerdType) + vN2ONIDMM(C,APS,APSLevel,HerdType) + vN2ONDeNitrM(C,APS,APSLevel,HerdType) + vN2ONDeNitrFeed(C,APS,APSLevel,HerdType)) * 44/28 * GHGequiv('N2O')))
;

** Cropland emissions **

equation eN2ONDCrop(C,Crop) "Direct N2O-N emissions from cropland";
         eN2ONDCrop(C,Crop)$CropInC(C,Crop)..
         vN2ONDCrop(C,Crop) =e=
                 sum(Crop1, (vFertResiduekg(C,Crop,Crop1)                                                               * FRV('Residue',Crop1,'N','Content')        * EFC(C,'Residue','N2ODS'))) +
                 sum((Z,Soil,LandType)$(CZSL(C,Z,Soil,LandType) and CropYieldha(LandType,Soil,Z,Crop,C)), vNumha(C,Z,Soil,LandType,Crop) * CropOther(Crop,'N')      * EFC(C,'Residue','N2ODS'))  +
                 sum((Z,Soil,LandType)$(CZSL(C,Z,Soil,LandType) and CropYieldha(LandType,Soil,Z,Crop,C)), vNumha(C,Z,Soil,LandType,Crop) * NMineral(Soil)           * EFC(C,'Mineral','N2ODS'))  +
                 sum(ProcOutCP, (vFertCoProductkg(C,Crop,ProcOutCP)                                                     * FRV('Co-Product',ProcOutCP,'N','Content') * EFC(C,'Residue','N2ODS'))) +
                 sum(APS, (vFertManure(C,Crop,APS,'N')                                                                                                              * EFC(C,'Manure','N2ODS')))  +
                 sum(Rumi, (vFertGrazing(C,Crop,Rumi,'N')$FreshGrass(Crop)                                                                                          * EFC(C,'Grazing','N2ODS'))) +
                (vFertFeedingLosskg(C,Crop)                                                                             * FRVC(C,'Feeding','N','Content')           * EFC(C,'Residue','N2ODS'))  +
                 sum(WasteSum, (vFertFoodWasteSumkg(C,Crop,WasteSum)                                                    * FRVC(C,WasteSum,'N','Content')            * EFC(C,'Residue','N2ODS'))) +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)), (vFertFoodWastekg(C,Crop,WasteByProd,ProcOutH)   * FRV('Foodwaste',ProcOutH,'N','Content')   * EFC(C,'Residue','N2ODS'))) +
                 sum(AllProd$ProdPHLoss(C,Allprod),                    (vFertFoodWastekg(C,Crop,PHLoss,AllProd)         * FRV('HarvestWaste',AllProd,'N','Content') * EFC(C,'Residue','N2ODS'))) +
                (vFertH(C,Crop,'N')                                                                                                                                 * EFC(C,'Sludge','N2ODS'))   +
                (vFertArtificial(C,Crop,'N')                                                                                                                        * EFC(C,'Artificial','N2ODS'))
;       
equation eNOXNCrop(C,Crop) " NOX-N emissions from cropland";
         eNOXNCrop(C,Crop)$CropInC(C,Crop)..
         vNOXNCrop(C,Crop) =e=
                 sum(Crop1, (vFertResiduekg(C,Crop,Crop1)                                                               * FRV('Residue',Crop1,'N','Content')        * EFC(C,'Residue','NOXS'))) +
                 sum((Z,Soil,LandType)$(CZSL(C,Z,Soil,LandType) and CropYieldha(LandType,Soil,Z,Crop,C)), vNumha(C,Z,Soil,LandType,Crop) * CropOther(Crop,'N')      * EFC(C,'Residue','NOXS'))  +
                 sum((Z,Soil,LandType)$(CZSL(C,Z,Soil,LandType) and CropYieldha(LandType,Soil,Z,Crop,C)), vNumha(C,Z,Soil,LandType,Crop) * NMineral(Soil)           * EFC(C,'Mineral','NOXS'))  +
                 sum(ProcOutCP, (vFertCoProductkg(C,Crop,ProcOutCP)                                                     * FRV('Co-Product',ProcOutCP,'N','Content') * EFC(C,'Residue','NOXS'))) +
                 sum(APS, (vFertManure(C,Crop,APS,'N')                                                                                                              * EFC(C,'Manure','NOXS')))  +
                 sum(Rumi, (vFertGrazing(C,Crop,Rumi,'N')$FreshGrass(Crop)                                                                                          * EFC(C,'Grazing','NOXS'))) +
                (vFertFeedingLosskg(C,Crop)                                                                             * FRVC(C,'Feeding','N','Content')           * EFC(C,'Residue','NOXS'))  +
                 sum(WasteSum, (vFertFoodWasteSumkg(C,Crop,WasteSum)                                                    * FRVC(C,WasteSum,'N','Content')            * EFC(C,'Residue','NOXS'))) +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)), (vFertFoodWastekg(C,Crop,WasteByProd,ProcOutH)   * FRV('Foodwaste',ProcOutH,'N','Content')   * EFC(C,'Residue','NOXS'))) +
                 sum(AllProd$ProdPHLoss(C,Allprod),                    (vFertFoodWastekg(C,Crop,PHLoss,AllProd)         * FRV('HarvestWaste',AllProd,'N','Content') * EFC(C,'Residue','NOXS'))) +
                (vFertH(C,Crop,'N')                                                                                                                                 * EFC(C,'Sludge','NOXS'))   +
                (vFertArtificial(C,Crop,'N')                                                                                                                        * EFC(C,'Artificial','NOXS'))
;       
equation eNH3NCrop(C,Crop) " NH3-N emissions from cropland";
         eNH3NCrop(C,Crop)$CropInC(C,Crop)..
         vNH3NCrop(C,Crop) =e=
                 sum(Crop1, (vFertResiduekg(C,Crop,Crop1)                                                               * FRV('Residue',Crop1,'N','Content')        * EFC(C,'Residue','NH3S'))) +
                 sum((Z,Soil,LandType)$(CZSL(C,Z,Soil,LandType) and CropYieldha(LandType,Soil,Z,Crop,C)), vNumha(C,Z,Soil,LandType,Crop) * CropOther(Crop,'N')      * EFC(C,'Residue','NH3S'))  +
                 sum((Z,Soil,LandType)$(CZSL(C,Z,Soil,LandType) and CropYieldha(LandType,Soil,Z,Crop,C)), vNumha(C,Z,Soil,LandType,Crop) * NMineral(Soil)           * EFC(C,'Mineral','NH3S'))  +
                 sum(ProcOutCP, (vFertCoProductkg(C,Crop,ProcOutCP)                                                     * FRV('Co-Product',ProcOutCP,'N','Content') * EFC(C,'Residue','NH3S'))) +
                 sum(APS, (vFertManure(C,Crop,APS,'N')                                                                                                              * EFC(C,'Manure','NH3S')))  +
                 sum(Rumi, (vFertGrazing(C,Crop,Rumi,'N')$FreshGrass(Crop)                                                                                          * EFC(C,'Grazing','NH3S'))) +
                (vFertFeedingLosskg(C,Crop)                                                                             * FRVC(C,'Feeding','N','Content')           * EFC(C,'Residue','NH3S'))  +
                 sum(WasteSum, (vFertFoodWasteSumkg(C,Crop,WasteSum)                                                    * FRVC(C,WasteSum,'N','Content')            * EFC(C,'Residue','NH3S'))) +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)), (vFertFoodWastekg(C,Crop,WasteByProd,ProcOutH)   * FRV('Foodwaste',ProcOutH,'N','Content')   * EFC(C,'Residue','NH3S'))) +
                 sum(AllProd$ProdPHLoss(C,Allprod),                    (vFertFoodWastekg(C,Crop,PHLoss,AllProd)         * FRV('HarvestWaste',AllProd,'N','Content') * EFC(C,'Residue','NH3S'))) +
                (vFertH(C,Crop,'N')                                                                                                                                 * EFC(C,'Sludge','NH3S'))   +
                (vFertArtificial(C,Crop,'N')                                                                                                                        * EFC(C,'Artificial','NH3S'))
;      
equation eNLeachCrop(C,Crop) " N leaching from cropland";
         eNLeachCrop(C,Crop)$CropInC(C,Crop)..
         vNLeachCrop(C,Crop) =e=
                 sum(Crop1, (vFertResiduekg(C,Crop,Crop1)                                                               * FRV('Residue',Crop1,'N','Content')        * EFC(C,'Residue','Leach'))) +
                 sum((Z,Soil,LandType)$(CZSL(C,Z,Soil,LandType) and CropYieldha(LandType,Soil,Z,Crop,C)), vNumha(C,Z,Soil,LandType,Crop) * CropOther(Crop,'N')      * EFC(C,'Residue','Leach'))  +
                 sum((Z,Soil,LandType)$(CZSL(C,Z,Soil,LandType) and CropYieldha(LandType,Soil,Z,Crop,C)), vNumha(C,Z,Soil,LandType,Crop) * NMineral(Soil)           * EFC(C,'Mineral','Leach'))  +
                 sum(ProcOutCP, (vFertCoProductkg(C,Crop,ProcOutCP)                                                     * FRV('Co-Product',ProcOutCP,'N','Content') * EFC(C,'Residue','Leach'))) +
                 sum(APS, (vFertManure(C,Crop,APS,'N')                                                                                                              * EFC(C,'Manure','Leach')))  +
                 sum(Rumi, (vFertGrazing(C,Crop,Rumi,'N')$FreshGrass(Crop)                                                                                          * EFC(C,'Grazing','Leach'))) +
                (vFertFeedingLosskg(C,Crop)                                                                             * FRVC(C,'Feeding','N','Content')           * EFC(C,'Residue','Leach'))  +
                 sum(WasteSum, (vFertFoodWasteSumkg(C,Crop,WasteSum)                                                    * FRVC(C,WasteSum,'N','Content')            * EFC(C,'Residue','Leach'))) +
                 sum((WasteByProd,ProcOutH)$(not PHLoss(WasteByProd)), (vFertFoodWastekg(C,Crop,WasteByProd,ProcOutH)   * FRV('Foodwaste',ProcOutH,'N','Content')   * EFC(C,'Residue','Leach'))) +
                 sum(AllProd$ProdPHLoss(C,Allprod),                    (vFertFoodWastekg(C,Crop,PHLoss,AllProd)         * FRV('HarvestWaste',AllProd,'N','Content') * EFC(C,'Residue','Leach'))) +
                (vFertH(C,Crop,'N')                                                                                                                                 * EFC(C,'Sludge','Leach'))   +
                (vFertArtificial(C,Crop,'N')                                                                                                                        * EFC(C,'Artificial','Leach'))
;
equation eN2ONLeachC(C,Crop) "N2O N leaching from cropland";
         eN2ONLeachC(C,Crop)$CropInC(C,Crop)..
         vN2ONLeachC(C,Crop) =e= vNLeachCrop(C,Crop) * EFC(C,'Other','N2OIDLS')
;       
equation eN2ONVolC(C,Crop) "N2O N Volatilisation from cropland";
         eN2ONVolC(C,Crop)$CropInC(C,Crop)..
         vN2ONVolC(C,Crop) =e= ((vNOXNCrop(C,Crop) * 14/30) + (vNH3NCrop(C,Crop) * 17/14)) * EFC(C,'Other','N2OIDVS')
;       
equation eCO2eqCropland(C,Crop) "N2O from cropland";
         eCO2eqCropland(C,Crop)$CropInC(C,Crop)..
         vCO2eqCropland(C,Crop) =e= (((vN2ONDCrop(C,Crop) + vN2ONLeachC(C,Crop) + vN2ONVolC(C,Crop)) * 44/28 * GHGequiv('N2O')) )
;

** Total emissions **

equation eCO2eqCountry(C) "Total CO2 emissions from crops, animals, transportation, and land use change";
         eCO2eqCountry(C)..
         vCO2eqCountry(C) =e=
                 sum(Crop$CropInC(C,Crop), vCO2eqCropland(C,Crop)) +
                 sum(APSAPSLevel, vCO2eqAnimal(C,APSAPSLevel)) +
                 vCO2Transport(C) + vCH4Transport(C)*GHGequiv('CH4Transp') + vN2OTransport(C)*GHGequiv('N2O') +
                 vCO2eqLUC(C)
;
equation eTotalCO2eq "Objective function minimize GHG emissions";
         eTotalCO2eq..
         vTotalCO2eq =e= sum(C, vCO2eqCountry(C))
;
equation eMaxCO2eqPerCapita(C) "Objective function minimize maximum GHG emissions per capita";
         eMaxCO2eqPerCapita(C)..
         vMaxCO2eqPerCapita =g= vCO2eqCountry(C) / Population(C)
;

****** Model and scenario definitions ******

*option limcol=100, limrow=100;
option solprint=off
option solver=cplex, threads=0;

model CFS / all /;

* Hack for writing GDX->Excel only (assumes existing output.tmp)
$label writeonly

$eval niterp1 %niter% + 1

sets
        Scenarios       "Available scenarios"   / MinLand, MinFert, MinGHG, MinPerCapGHG, MinImport /   // no MinImport yet
        IterNext        "Iteration counter including one more iteration" / 1*%niterp1% /
        Iter(IterNext)  "Iteration counter, stop after %niter% iterations at the latest" / 1*%niter% /
;

singleton set ActuallyRun(Scenarios) "Model to actually run" / %model% /;

* Hack for writing GDX->Excel only (assumes existing output.tmp)
$if set writeonly $goto reportingloop

scalars     Delta           "Relative change of the quantities calculated at each iteration"
            Delta_eps       "Delta stopping criterion: stop iteration if largest relative parameter update < Delta_eps"   / 0.1 /
            Delta_last      "Largest relative parameter update if iteration would continue"
            Current_iter    "Last iteration"
;

parameters  Delta_max(Iter) "Maximum relative parameter update after given iteration"
;

parameters  AniNutrByProductCIter   (IterNext,C,LossType,AniNutr)   "Animal nutrients in aggregated waste in nutrient units per kg DM (per iteration)"
            FeedingLossFractionIter (IterNext,C)                    "Feeding loss fraction for aggregated waste animal feed (per iteration)"
            FRVCIter                (IterNext,C,*,FertNutr,ConFRV)  "Mineral fertiliser replacement values and content in nutrient units per kg for aggregated waste (per iteration)"
;


****** Reporting sets and parameters ******

sets
        FertTypeR                   "Fertilizer types for reporting" / Artificial, Manure, GrazingManure, Crop-Residue, Co-Product, FoodWaste, FeedWaste, HumanExcreta/
        ProdAndWaste                "AllProd and aggregated waste" / #AllProd, #WasteSum /
        AniProdFamWaste             "Animal product families plus waste" / #AniProdFam, 'Waste' /
;

parameters
        rDailyIntakeg                   (C,ProdFam,AllProd)         "Daily per capita food intake by product family and food product in grams"
        rDailyIntakeFamilyg             (C,ProdFam)                 "Daily per capita food intake by product family in grams"
        rNutrIntake                     (C,HumNutr,*)               "Daily per capita nutrition intake in nutrient units"
        rHumanDiet                      (C,MinMax,ProdFam)          "Human diet fulfilment in percent of minimum and maximum values"
        rFoodExports                    (C,C)                       "Amount of food staying in the country (diagonal) and food exported (off diagonal) before consumption loss in tons"
        rFoodExportsPct                 (C,C)                       "Amount of food staying in the country (diagonal) and food exported (off diagonal) in percent of food produced (before consumption loss)"
        rFoodImportsPct                 (C,C)                       "Amount of food staying in the country (diagonal) and food imported (off diagonal) in percent of food available at destination (before consumption loss)"
        rCoProdExports                  (C,C)                       "Amount of co-product used in the country (diagonal) and co-product exported (off diagonal) in tons"
        rCoProdExportsPct               (C,C)                       "Amount of co-product used in the country (diagonal) and co-product exported (off diagonal) in percent of co-product produced"
        rCoProdImportsPct               (C,C)                       "Amount of co-product used in the country (diagonal) and co-product imported (off diagonal) in percent of co-product available at destination"
        rCrophaC                        (Crop,C)                    "Land area used by crop and country in ha"
        rCrophaCPct                     (Crop,C)                    "Land area used by crop and country in percent of land with nonzero yield"
        rCrophaZ                        (Crop,C,Z)                  "Land area used by crop, country, and zone in ha"
        rCrophaZPct                     (Crop,C,Z)                  "Land area used by crop, country, and zone in percent of land with nonzero yield"
        rLandhaZ                        (LandType,C,Z)              "Land area used by land type, country, and zone in ha"
        rLandhaZPct                     (LandType,C,Z)              "Land area used by land type, country, and zone in percent of available land (-1 if no land available)"
        rLandhaC                        (LandType,C)                "Land area used by land type and country in ha"
        rLandhaCPct                     (LandType,C)                "Land area used by land type and country in percent of available land (-1 if no land available)"
        rYieldOverall                   (C,Crop)                    "Overall yield for crop in country in tons per ha"
        rExportCropLandha               (C,C)                       "Cropland used for domestic crops (diagonal) and exported crops (off diagonal)in ha"
        rExportCropLandPct              (C,C)                       "Cropland used for domestic crops (diagonal) and exported crops (off diagonal)in percent of used land"
        
        rFertiliser                     (C,FertTypeR,FertNutr)      "Amount of fertiliser nutrients in kg by fertiliser type and country"
        rFertiliserSurplus              (C,FertNutr)                "Surplus fertiliser by country in kg"
        rFertiliserSurplusPct           (C,FertNutr)                "Surplus fertiliser by country in percent of required fertiliser"
        rFertArtificial                 (C,FertNutr)                "Used artificial fertiliser in kg"
        rFertResidue                    (C,Crop,FertNutr)           "Amount of FertNutr originating from crop residue in kg"
        rFertCoProduct                  (C,AllProd,FertNutr)        "Amount of FertNutr originating from co-products in kg"
        rFertManure                     (C,APS,FertNutr)            "Amount of FertNutr originating from APS manure in kg"
        rFertGrazingManure              (C,APS,FertNutr)            "Amount of FertNutr originating from ruminants' grazing manure in kg"
        rFertFeedWaste                  (C,FertNutr)                "Amount of FertNutr originating from feeding waste in kg"
        rFertFoodWaste                  (C,ProdAndWaste,FertNutr)   "Amount of FertNutr originating from all food waste in kg"
        rFertHumanExcreta               (C,FertNutr)                "Amount of FertNutr originating from human excreta in kg"
        rFertReq                        (C,FertNutr)                "Fertiliser required by country in kg"
        rFertiliserPerc                 (C,FertTypeR,FertNutr)      "Origin of fertiliser nutrients fertiliser type and country, given in percent of total fertiliser by country"

        rNumHerdTypeC                   (APS,APSLevel,HerdType,C)   "Number of animals per country"
*        NumHerdTypeR(APS,Level,HerdType,C,Z)
        rNumProducerC                   (APS,APSLevel,HerdType,C)   "Number of animal producers per country"
*        NumProducerR(APS,Level,HerdType,C,Z)
        rCoProdIntakeHerdTypekgDM       (C,APS,APSLevel,HerdType,AllProd)               "Animal co-product intake"
        rGrassIntakeHerdTypekgDM        (C,APS,APSLevel,HerdType,AllProd)               "Animal grass intake"
        rAllFoodWasteIntakeHerdTypekgDM (C,APS,APSLevel,HerdType,LossType,ProdAndWaste) "Food waste animal intake"
        rIntakeHerdTypekgDM             (C,APS,APSLevel,HerdType,ProdAndWaste)          "Total animal intake incl. aggregated waste in kg DM"
        rIntakeHerdTypeFamkgDM          (C,APS,APSLevel,HerdType,AniProdFamWaste)       "Per-family animal intake"

        rAnimalCO2eqCapita              (C,APS,APSLevel,*)          "Animal emissions in kg CO2 equivalent per human capita"
        rCroplandEmissionsCapita        (C,Crop,*)                  "Crop emissions in kg CO2 equivalent per human capita"
        rTransportationCO2eq            (C)                         "CO2 equiv emissions from transportation, assigned to country of origin"
        rTotalEmissionsCapita           (C,*)                       "Total emissions in kg CO2 equivalent per human capita"

* Avoid setting variable attributes to eps -> copy to report parameters
        rNumha                          (C,Z,Soil,LandType,Crop)                    "Number of hectars used"
        rFoodWasteAv                    (C,LossType,AllProd)                        "Waste available in tons"
        rFertFoodWastekg                (C,Crop,LossType,AllProd)                   "Food waste applied as fertiliser in kg (waste of individual products)"
        rFertFoodWasteSumkg             (C,Crop,LossType)                           "Food waste applied as fertiliser in kg (waste of aggregated products)"
        rFoodWasteIntakeHerdTypekgDM    (C,APS,APSLevel,HerdType,LossType,AllProd)  "Intake of food waste (individual products) per animal herd type in kg DM"
        rFoodWasteIntakeHerdTypeSumkgDM (C,APS,APSLevel,HerdType,LossType)          "Intake of food waste (aggregated products) per animal herd type in kg DM"

$ontext
        AniNutrByProductTest(WasteCat,AllProd,AniNutr)
        ScenarioResults(*)
        AnimalCO2eqCapita(APS,Level,*)
        CroplandEmissionsCapita(SoilType,Crop,*)
*        NSurplus(C,Z,LandType,SoilType,Crop)
*        NSurplus(C,Z,LandType,Soiltype,Crop,FertNutr)
        AniNutrByProductFMCR(C,Z,WasteCat,Waste,AllNutr)
        FertSurplus2(C,Z,LandType,Soiltype,Crop,FertNutr)
        TestNLossCap(APS,Level,HerdType)
        TestManureCap(APS,Level,HerdType)
$offtext
;

$onEcho > output.tmp
EpsOut=0
*squeeze=n
  text="GAMS command line parameters"    rng=Settings!A1
   set=settings                          rng=Settings!A3            rdim=1
*   par=ScenarioResults                   rng=ScenarioResults!A1
*textID=DailyIntakeGramsPerCapitaPerDay   rng=DailyIntake!A1
textID=rDailyIntakeg                     rng=DailyIntake!A1
   par=rDailyIntakeg                     rng=DailyIntake!A3         rdim=1 cdim=2
textID=rDailyIntakeFamilyg               rng=DailyIntakeFamily!A1
   par=rDailyIntakeFamilyg               rng=DailyIntakeFamily!A3   rdim=1 cdim=1
*textID=DailyIntakeNutrPerCapita:VariousUnits                       rng=NutrIntake!A1
textID=rNutrIntake                       rng=NutrIntake!A1
   par=rNutrIntake                       rng=NutrIntake!A3          rdim=2 cdim=1
textID=rHumanDiet                        rng=HumanDiet!A1
   par=rHumanDiet                        rng=HumanDiet!A3           rdim=1 cdim=2
textID=rFoodExports                      rng=FoodExports!A1
   par=rFoodExports                      rng=FoodExports!A3         rdim=1 cdim=1
textID=rFoodExportsPct                   rng=FoodExportsPct!A1
   par=rFoodExportsPct                   rng=FoodExportsPct!A3      rdim=1
textID=rFoodImportsPct                   rng=FoodImportsPct!A1
   par=rFoodImportsPct                   rng=FoodImportsPct!A3      rdim=1
textID=rCoProdExports                    rng=CoProdExports!A1
   par=rCoProdExports                    rng=CoProdExports!A3       rdim=1 cdim=1
textID=rCoProdExportsPct                 rng=CoProdExportsPct!A1
   par=rCoProdExportsPct                 rng=CoProdExportsPct!A3    rdim=1
textID=rCoProdImportsPct                 rng=CoProdImportsPct!A1
   par=rCoProdImportsPct                 rng=CoProdImportsPct!A3    rdim=1
*textID=CroplandHectarePerCountry         rng=CrophaCountry!A1
textID=rCrophaC                          rng=CrophaCountry!A1
   par=rCrophaC                          rng=CrophaCountry!A3       rdim=1 cdim=1
textID=rCrophaCPct                       rng=CrophaCountryPct!A1
   par=rCrophaCPct                       rng=CrophaCountryPct!A3    rdim=1 cdim=1
*textID=CroplandHectarePerZone            rng=CrophaZone!A1
textID=rCrophaZ                          rng=CrophaZone!A1
   par=rCrophaZ                          rng=CrophaZone!A3          rdim=1 cdim=2
textID=rCrophaZPct                       rng=CrophaZonePct!A1
   par=rCrophaZPct                       rng=CrophaZonePct!A3       rdim=1 cdim=2
*textID=LandAreaPerCountry                rng=LandTypehaCountry!A1
textID=rLandhaC                          rng=LandTypehaCountry!A1
   par=rLandhaC                          rng=LandTypehaCountry!A3   rdim=1 cdim=1
textID=rLandhaCPct                       rng=LandTypehaCountryPct!A1
   par=rLandhaCPct                       rng=LandTypehaCountryPct!A3 rdim=1 cdim=1
*textID=LandAreaPerZone                   rng=LandTypehaZone!A1
textID=rLandhaZ                          rng=LandTypehaZone!A1
   par=rLandhaZ                          rng=LandTypehaZone!A3      rdim=1 cdim=2
textID=rLandhaZPct                       rng=LandTypehaZonePct!A1
   par=rLandhaZPct                       rng=LandTypehaZonePct!A3   rdim=1 cdim=2
textID=rExportCropLandha                 rng=ExportCropLandha!A1
   par=rExportCropLandha                 rng=ExportCropLandha!A3    rdim=1 cdim=1
textID=rExportCropLandPct                rng=ExportCropLandPct!A1
   par=rExportCropLandPct                rng=ExportCropLandPct!A3   rdim=1 cdim=1
*textID=FertiliserKgPerCountry            rng=Fertliser!A1
textID=rFertiliserPerc                   rng=Fertliser!A1
   par=rFertiliserPerc                   rng=Fertliser!A3           rdim=2 cdim=1
textID=rFertReq                          rng=FertliserRequired!A1
   par=rFertReq                          rng=FertliserRequired!A3
*textID=FertiliserSurplusKgPerCountry     rng=FertiliserSurplus!A1
textID=rFertiliserSurplus                rng=FertiliserSurplus!A1
   par=rFertiliserSurplus                rng=FertiliserSurplus!A3
textID=rFertiliserSurplusPct             rng=FertiliserSurplusPct!A1
   par=rFertiliserSurplusPct             rng=FertiliserSurplusPct!A3
*textID=NumberOfHerdTypesPerCountry       rng=NumHerdTypeCountry!A1
textID=rNumHerdTypeC                     rng=NumHerdTypeCountry!A1
   par=rNumHerdTypeC                     rng=NumHerdTypeCountry!A3  cdim=1
*par=NumHerdTypeR                      rng=NumHerdTypeRegion!A1   cdim=2
*textID=NumberOfProducersPerCountry       rng=NumProducerCountry!A1
textID=rNumProducerC                     rng=NumProducerCountry!A1
   par=rNumProducerC                     rng=NumProducerCountry!A3  cdim=1
*par=NumProducerR                      rng=NumProducerRegion!A1   cdim=2
*textID=AnnualIntakeOfAnimalsInKgDM       rng=IntakeHerdTypekgDM!A1
textID=rIntakeHerdTypekgDM               rng=IntakeHerdTypekgDM!A1
   par=rIntakeHerdTypekgDM               rng=IntakeHerdTypekgDM!A3
*textID=AnimalNutrientsInWasteUpdated     rng=AniNutrByProductC!A1   
textID=AniNutrByProductC                 rng=AniNutrByProductC!A1   
   par=AniNutrByProductC                 rng=AniNutrByProductC!A3
*textID=FRVAndNutrientContentOfWasteg/Kg  rng=FRVC!A1
textID=FRVC                              rng=FRVC!A1
   par=FRVC                              rng=FRVC!A3
*textID=GrazingProportionUpdated          rng=GrazingProportion!A1
textID=GrazingProportion                 rng=GrazingProportion!A1
   par=GrazingProportion                 rng=GrazingProportion!A3
*   var=vNumHerdType                      rng=vNumHerdType!A1
textID=rNumha                            rng=rNumha!A1
   par=rNumha                            rng=rNumha!A3
*   par=TestNLossCap                      rng=TestNLossCap!A1
*   par=TestManureCap                     rng=TestManureCap!A1
*   par=AnimalCO2eqCapita                 rng=AnimalCO2eqCapita!A1
*   par=NSurplus                          rng=NSurplus!A1
*   par=FoodWasteIntakeHerdTypekgFM       rng=FoodWasteIntakeHerdTypekgFM!A1
textID=rFoodWasteAv                      rng=rFoodWasteAv!A1
   par=rFoodWasteAv                      rng=rFoodWasteAv!A3
textID=rFertFoodWastekg                  rng=rFertFoodWastekg!A1
   par=rFertFoodWastekg                  rng=rFertFoodWastekg!A3
textID=rFertFoodWasteSumkg               rng=rFertFoodWasteSumkg!A1
   par=rFertFoodWasteSumkg               rng=rFertFoodWasteSumkg!A3
textID=rFoodWasteIntakeHerdTypekgDM      rng=rFoodWasteIntakeHerdTypekgDM!A1
   par=rFoodWasteIntakeHerdTypekgDM      rng=rFoodWasteIntakeHerdTypekgDM!A3
textID=rFoodWasteIntakeHerdTypeSumkgDM   rng=rFoodWasteIntakeHerdTypeSumkgDM!A1
   par=rFoodWasteIntakeHerdTypeSumkgDM   rng=rFoodWasteIntakeHerdTypeSumkgDM!A3
*TextID=AnimalEmissionsKgCO2ePerHumanCapita rng=rAnimalCO2eqCapita!A1   
TextID=rAnimalCO2eqCapita                rng=rAnimalCO2eqCapita!A1   
   par=rAnimalCO2eqCapita                rng=rAnimalCO2eqCapita!A3
*TextID=CropEmissionskgCO2ePerHumanCapita  rng=rCroplandEmissionsCapita!A1
TextID=rCroplandEmissionsCapita          rng=rCroplandEmissionsCapita!A1
   par=rCroplandEmissionsCapita          rng=rCroplandEmissionsCapita!A3
*TextID=TotalEmissionkgCO2ePerHumanCapita rng=rTotalEmissionsCapita!A1
TextID=rTotalEmissionsCapita             rng=rTotalEmissionsCapita!A1
   par=rTotalEmissionsCapita             rng=rTotalEmissionsCapita!A3
$offEcho

AniNutrByProductCIter(Iter,C,WasteSum,AniNutr)$(ord(Iter)=1)    = AniNutrByProductC(C,WasteSum,AniNutr);
FeedingLossFractionIter(Iter,C)$(ord(Iter)=1)                   = FeedingLossFraction(C);
FRVCIter(Iter,C,WasteSum,FertNutr,ConFRV)$(ord(Iter)=1)         = FRVC(C,WasteSum,FertNutr,ConFRV);
FRVCIter(Iter,C,FLoss,FertNutr,ConFRV)$(ord(Iter)=1)            = FRVC(C,FLoss,FertNutr,ConFRV);

Current_iter = 0;

* Hack for writing GDX->Excel only (assumes existing output.tmp)
$label reportingloop

loop(Iter,

* Hack for writing GDX->Excel only (assumes existing output.tmp)
$if set writeonly $goto writeexcelreport

    Current_iter = Current_iter + 1;

    if( sameas(ActuallyRun,'MinLand'),
        solve CFS minimizing vTotalLandUse using lp;
    elseif sameas(ActuallyRun,'MinFert'),
        solve CFS minimizing vTotalArtFert using lp;
   elseif sameas(ActuallyRun,'MinGHG'),
        solve CFS minimizing vTotalCO2eq using lp;
    elseif sameas(ActuallyRun,'MinPerCapGHG'),
        solve CFS minimizing vMaxCO2eqPerCapita using lp;
    else
        abort "Unknown model"
    );

****** Reporting start ******
****** Note that reporting parameters need to be reset to eliminate all eps values between iterations ******
****** This is important for parameters that may be used in calculating subsequent parameters where eps values may cause problems ******

** Green house gas emissions **

    rAnimalCO2eqCapita(C,APS,APSLevel,'MethaneManure')                 = 0;
    rAnimalCO2eqCapita(C,Rumi,APSLevel,'MethaneEnteric')               = 0;
    rAnimalCO2eqCapita(C,APS,APSLevel,'DirectNitrousOxideManure')      = 0;
    rAnimalCO2eqCapita(C,APS,APSLevel,'IndirectNitrousOxideManure')    = 0;
    rAnimalCO2eqCapita(C,APS,APSLevel,'TotalCO2Animal')                = 0;
    rAnimalCO2eqCapita(C,APS,APSLevel,'MethaneManure')                 = SUM((HerdType), vCH4Manure.L(C,APS,APSLevel,HerdType)$vCH4Manure.L(C,APS,APSLevel,HerdType) * GHGequiv('CH4')) / Population(C);
    rAnimalCO2eqCapita(C,Rumi,APSLevel,'MethaneEnteric')               = SUM((HerdType), vCH4Enteric.L(C,Rumi,APSLevel,HerdType)$vCH4Enteric.L(C,Rumi,APSLevel,HerdType) * GHGequiv('CH4')) / Population(C);
    rAnimalCO2eqCapita(C,APS,APSLevel,'DirectNitrousOxideManure')      = SUM((HerdType), vN2ONIDMM.L(C,APS,APSLevel,HerdType)$vN2ONIDMM.L(C,APS,APSLevel,HerdType) * 44/28 * GHGequiv('N2O')) / Population(C);
    rAnimalCO2eqCapita(C,APS,APSLevel,'IndirectNitrousOxideManure')    = SUM((HerdType), vN2ONIDMM.L(C,APS,APSLevel,HerdType)$vN2ONIDMM.L(C,APS,APSLevel,HerdType) * 44/28 * GHGequiv('N2O')) / Population(C);
    rAnimalCO2eqCapita(C,APS,APSLevel,'TotalCO2Animal')                = vCO2eqAnimal.L(C,APS,APSLevel)$vCO2eqAnimal.L(C,APS,APSLevel) / Population(C);
    
    rCroplandEmissionsCapita(C,Crop,'DirectNitrousOxideManureCropland')     = 0;
    rCroplandEmissionsCapita(C,Crop,'InDirectNitrousOxideManureCropland')   = 0;
    rCroplandEmissionsCapita(C,Crop,'DirectNitrousOxideManureCropland')     = (vN2ONDCrop.L(C,Crop)$vN2ONDCrop.L(C,Crop) * 44/28 * GHGequiv('N2O'))  / Population(C);
    rCroplandEmissionsCapita(C,Crop,'InDirectNitrousOxideManureCropland')   = (vN2ONLeachC.L(C,Crop)$vN2ONLeachC.L(C,Crop) +  vN2ONVolC.L(C,Crop)$vN2ONVolC.L(C,Crop)) * 44/28 * GHGequiv('N2O') / Population(C);
*    rCroplandEmissionsCapita(Crop,'NH3Cropland')     = (vNH3NCrop.L(C,Crop)$vNH3NCrop.L(CR,LandType,SoilType,Crop) * 14/30)) / SUM((CR,Gender,Age), Population(CR,Gender,Age));
*    rCroplandEmissionsCapita(Crop,'NOxCropland')     = (vNH3NCrop.L(C,Crop)$vNOXNCrop.L(CR,LandType,SoilType,Crop)  * 17/14)) / SUM((CR,Gender,Age), Population(CR,Gender,Age));
*    rCroplandEmissionsCapita(Crop,'Leaching')        = (vNH3NCrop.L(C,Crop)$vNLeachCrop.L(CR,LandType,SoilType,Crop)))       / SUM((CR,Gender,Age), Population(CR,Gender,Age));
    rCroplandEmissionsCapita(C,Crop,'TotalCO2Crop')       = 0;
    rCroplandEmissionsCapita(C,Crop,'TotalCO2Crop')       = vCO2eqCropland.L(C,Crop)$vCO2eqCropland.L(C,Crop) / Population(C);
    
    rTotalEmissionsCapita(C,'CO2eqPSF')    = 0;
    rTotalEmissionsCapita(C,'CO2eqASF')    = 0;
    rTotalEmissionsCapita(C,'CO2eqTrans')     = 0;
    rTotalEmissionsCapita(C,'CO2eqLUChange')  = 0;
    rTotalEmissionsCapita(C,'CO2eqTotal')     = 0;
    rTotalEmissionsCapita(C,'CO2eqPSF')    = SUM(Crop, vCO2eqCropland.L(C,Crop)$vCO2eqCropland.L(C,Crop)) /  Population(C);
    rTotalEmissionsCapita(C,'CO2eqASF')    = SUM((APS,APSLevel),    vCO2eqAnimal.L(C,APS,APSLevel)$vCO2eqAnimal.L(C,APS,APSLevel)) / Population(C);
    rTotalEmissionsCapita(C,'CO2eqTrans')     = (vCO2Transport.L(C)$vCO2Transport.L(C) +  vCH4Transport.L(C)$vCH4Transport.L(C) *GHGequiv('CH4Transp') + vN2OTransport.L(C)$vN2OTransport.L(C) * GHGequiv('N2O'))  /  Population(C);
    rTotalEmissionsCapita(C,'CO2eqLUChange')  = vCO2eqLUC.L(C)$vCO2eqLUC.L(C) / Population(C);
    rTotalEmissionsCapita(C,'CO2eqTotal')     = vCO2eqCountry.L(C)$vCO2eqCountry.L(C)   / Population(C);

** Human nutrient intake and human diet **

    rDailyIntakeg(C,ProdFam,ProcOutH)$FP(ProdFam,ProcOutH) = 0;
    rDailyIntakeFamilyg(C,ProdFam) = 0;
    rNutrIntake(C,HumNutr,'Total') = 0;
    rNutrIntake(C,HumNutr,ProdFam) = 0;
    rNutrIntake(C,HumNutr,'% of min requirement')$HumNutrReq('Min',HumNutr) = 0;
    rNutrIntake(C,HumNutr,'% of max requirement')$HumNutrReq('Max',HumNutr) = 0;
    rHumanDiet(C,MinMax,ProdFam)$HumDiet(MinMax,ProdFam) = 0;
    rDailyIntakeg(C,ProdFam,ProcOutH)$FP(ProdFam,ProcOutH) = vConQtyH.l(C,ProcOutH) * 1e6 / 365 / Population(C);
    rDailyIntakeFamilyg(C,ProdFam) = sum(ProcOutH$FP(ProdFam,ProcOutH), vConQtyH.l(C,ProcOutH)) * 1e6 / 365 / Population(C);
    rNutrIntake(C,HumNutr,'Total') = sum(ProcOutH, vConQtyH.l(C,ProcOutH) * HumNutrByProduct(ProcOutH,HumNutr)) * 1e4 / 365 / Population(C);
    rNutrIntake(C,HumNutr,ProdFam) = sum(ProcOutH$FP(ProdFam,ProcOutH), vConQtyH.l(C,ProcOutH) * HumNutrByProduct(ProcOutH,HumNutr)) * 1e4 / 365 / Population(C);
    rNutrIntake(C,HumNutr,'% of min requirement')$HumNutrReq('Min',HumNutr) = rNutrIntake(C,HumNutr,'Total') / HumNutrReq('Min',HumNutr) * 100;
    rNutrIntake(C,HumNutr,'% of max requirement')$HumNutrReq('Max',HumNutr) = rNutrIntake(C,HumNutr,'Total') / HumNutrReq('Max',HumNutr) * 100;
    rHumanDiet(C,MinMax,ProdFam)$HumDiet(MinMax,ProdFam) = rDailyIntakeFamilyg(C,ProdFam) / HumDiet(MinMax,ProdFam) * 100;

$iftheni "%procInOut%" == "on"
    rFoodExports(C,C1)      = 0;
    rFoodExportsPct(C,C1)   = 0;
    rFoodImportsPct(C,C1)   = 0;
    rFoodExports(C,C1)      = sum(ProcOutH,vFromTo.l(C,C1,ProcOutH));
    rFoodExportsPct(C,C1)   = rFoodExports(C,C1) / sum(ProcOutH, vQtyAvH.l(C,ProcOutH)) * 100;
    rFoodImportsPct(C,C1)   = rFoodExports(C,C1) / sum(ProcOutH, vConQtyH.l(C1,ProcOutH)) * 100;

    rCoProdExports(C,C1)    = 0;
    rCoProdExportsPct(C,C1) = 0;
    rCoProdImportsPct(C,C1) = 0;
    rCoProdExports(C,C1)    = sum(ProcOutCP, vFromTo.l(C,C1,ProcOutCP));
    rCoProdExportsPct(C,C1) = rCoProdExports(C,C1) / sum(ProcOutCP, vQtyProcOut.l(C,ProcOutCP)) * 100;
    rCoProdImportsPct(C,C1) = rCoProdExports(C,C1) / sum(ProcOutCP, vCPTotal.l(C1,ProcOutCP)) * 100;
$endif

** Land area **

    rCrophaC(Crop,C)                        = 0;
    rCrophaCPct(Crop,C)                     = 0;
    rCrophaZPct(Crop,C,Z)                   = 0;
    rLandhaC(LandType,C)                    = 0;
    rLandhaCPct(LandType,C)                 = 0;
    rLandhaZ(LandType,C,Z)                  = 0;
    rLandhaZPct(LandType,C,Z)               = 0;
    rCrophaC(Crop,C)                        = sum((Z,Soil,LandType), vNumha.l(C,Z,Soil,LandType,Crop));
    rCrophaCPct(Crop,C)$sum((LandType,Soil,Z)$CropYieldha(LandType,Soil,Z,Crop,C),LandAreaha(LandType,Soil,Z,C)) =
                                              rCrophaC(Crop,C) / sum((LandType,Soil,Z)$CropYieldha(LandType,Soil,Z,Crop,C),LandAreaha(LandType,Soil,Z,C)) * 100;
    rCrophaZ(Crop,C,Z)                      = sum((Soil,LandType), vNumha.l(C,Z,Soil,LandType,Crop));
    rCrophaZPct(Crop,C,Z)$sum((LandType,Soil)$CropYieldha(LandType,Soil,Z,Crop,C),LandAreaha(LandType,Soil,Z,C)) =
                                              rCrophaZ(Crop,C,Z) / sum((LandType,Soil)$CropYieldha(LandType,Soil,Z,Crop,C),LandAreaha(LandType,Soil,Z,C)) * 100;
    rLandhaC(LandType,C)                    = sum((Z,Soil,Crop), vNumha.l(C,Z,Soil,LandType,Crop));
    rLandhaCPct(LandType,C)$sum((Soil,Z), LandAreaha(LandType,Soil,Z,C)) =
                                              rLandhaC(LandType,C) / sum((Soil,Z),LandAreaha(LandType,Soil,Z,C)) * 100;
    rLandhaCPct(LandType,C)$(not sum((Soil,Z), LandAreaha(LandType,Soil,Z,C))) =
                                              -1;
    rLandhaZ(LandType,C,Z)                  = sum((Soil,Crop), vNumha.l(C,Z,Soil,LandType,Crop));
    rLandhaZPct(LandType,C,Z)$sum(Soil, LandAreaha(LandType,Soil,Z,C)) =
                                              rLandhaZ(LandType,C,Z) / sum(Soil, LandAreaha(LandType,Soil,Z,C)) * 100;
    rLandhaZPct(LandType,C,Z)$(not sum(Soil, LandAreaha(LandType,Soil,Z,C))) =
                                              -1;

    rYieldOverall(C,Crop)$rCrophaC(Crop,C)  = 0;
    rYieldOverall(C,Crop)$rCrophaC(Crop,C)  = vCropAv.l(C,Crop) / rCrophaC(Crop,C);

$iftheni "%cropInOut%" == "on"
    rExportCropLandha(C,C1)     = 0;
    rExportCropLandPct(C,C1)    = 0;
    rExportCropLandha(C,C1)$sum(Crop, rCrophaC(Crop,C)) =
                                              sum(Crop$(vFromToCrop.l(C,C1,Crop)>1), vFromToCrop.l(C,C1,Crop)/(1-LossFraction('Crop',Crop,C,PHLoss))/rYieldOverall(C,Crop));
    rExportCropLandPct(C,C1)$sum(Crop, rCrophaC(Crop,C)) =
                                              sum(Crop$(vFromToCrop.l(C,C1,Crop)>1), vFromToCrop.l(C,C1,Crop)/(1-LossFraction('Crop',Crop,C,PHLoss))/rYieldOverall(C,Crop)) / sum(Crop, rCrophaC(Crop,C)) * 100;
$endif

** Fertiliser **

    rFertArtificial(C,FertNutr)             = 0;
    rFertResidue(C,Crop,FertNutr)           = 0;
    rFertCoProduct(C,ProcOutCP,FertNutr)    = 0;
    rFertManure(C,APS,FertNutr)             = 0;
    rFertGrazingManure(C,Rumi,FertNutr)     = 0;
    rFertFeedWaste(C,FertNutr)              = 0;
    rFertFoodWaste(C,ProdAndWaste,FertNutr) = 0;
    rFertArtificial(C,FertNutr)             = sum(Crop, vFertArtificial.l(C,Crop,FertNutr));
    rFertResidue(C,Crop,FertNutr)           = sum(Crop1, vFertResiduekg.l(C,Crop1,Crop))                  * FRV('Residue',Crop,FertNutr,'Content')         * FRV('Residue',Crop,FertNutr,'FRV');
    rFertCoProduct(C,ProcOutCP,FertNutr)    = sum(Crop, vFertCoProductkg.l(C,Crop,ProcOutCP))             * FRV('Co-Product',ProcOutCP,FertNutr,'Content') * FRV('Co-Product',ProcOutCP,FertNutr,'FRV');
    rFertManure(C,APS,FertNutr)             = sum(Crop, vFertManure.l(C,Crop,APS,FertNutr))               * FRV('Manure',APS,FertNutr,'FRV');
    rFertGrazingManure(C,Rumi,FertNutr)     = sum(CropGrass, vFertGrazing.l(C,CropGrass,Rumi,FertNutr))   * FRV('Manure',Rumi,FertNutr,'FRV');
    rFertFeedWaste(C,FertNutr)              = sum(Crop, vFertFeedingLosskg.l(C,Crop))                     * FRVC(C,FLoss,FertNutr,'Content')               * FRVC(C,FLoss,FertNutr,'FRV');
* The following is split in two parts: (i) aggregated waste intake and (ii) per-product waste intake
    rFertFoodWaste(C,ProdAndWaste,FertNutr) =
        sum((WasteSum,Crop)$diag(WasteSum,ProdAndWaste), vFertFoodWasteSumkg.l(C,Crop,WasteSum)          * FRVC(C,WasteSum,FertNutr,'Content')            * FRVC(C,WasteSum,FertNutr,'FRV')) +
* Important: if there are postharvest waste products with the same name as food products their waste nutrients will be aggregated in the report!
        sum((AllProd,Crop)$diag(AllProd,ProdAndWaste),
            sum(WasteByProd$(not PHLoss(WasteByProd)),   vFertFoodWastekg.l(C,Crop,WasteByProd,AllProd)) * FRV('Foodwaste',AllProd,FertNutr,'Content')    * FRV('Foodwaste',AllProd,FertNutr,'FRV') +
            sum(WasteByProd$PHLoss(WasteByProd),         vFertFoodWastekg.l(C,Crop,WasteByProd,AllProd)) * FRV('HarvestWaste',AllProd,FertNutr,'Content') * FRV('HarvestWaste',AllProd,FertNutr,'FRV')
        );


    rFertHumanExcreta(C,FertNutr)           = 0;
    rFertHumanExcreta(C,FertNutr)           = sum(Crop, vFertH.l(C,Crop,FertNutr));

    rFertiliser(C,FertTypeR,FertNutr)       = 0;
    rFertiliser(C,'Artificial',FertNutr)    = rFertArtificial(C,FertNutr);
    rFertiliser(C,'Crop-Residue',FertNutr)  = sum(Crop, rFertResidue(C,Crop,FertNutr));
    rFertiliser(C,'Manure',FertNutr)        = sum(APS, rFertManure(C,APS,FertNutr));
    rFertiliser(C,'GrazingManure',FertNutr) = sum(Rumi, rFertGrazingManure(C,Rumi,FertNutr));
    rFertiliser(C,'Co-Product',FertNutr)    = sum(ProcOutCP, rFertCoProduct(C,ProcOutCP,FertNutr));
    rFertiliser(C,'FeedWaste',FertNutr)     = rFertFeedWaste(C,FertNutr);
    rFertiliser(C,'FoodWaste',FertNutr)     = sum(ProdAndWaste, rFertFoodWaste(C,ProdAndWaste,FertNutr));
    rFertiliser(C,'HumanExcreta',FertNutr)  = rFertHumanExcreta(C,FertNutr);

    rFertReq(C,FertNutr)                    = 0;
    rFertiliserSurplus(C,FertNutr)          = 0;
    rFertiliserSurplusPct(C,FertNutr)       = 0;
    rFertiliserPerc(C,FertTypeR,FertNutr)   = 0;
    rFertReq(C,FertNutr)                    = sum((Z,Soil,LandType,Crop), CropFertkg(LandType,Soil,Z,Crop,FertNutr,C) * vNumha.l(C,Z,Soil,LandType,Crop));
    rFertiliserSurplus(C,FertNutr)          = sum(FertTypeR, rFertiliser(C,FertTypeR,FertNutr)) - rFertReq(C,FertNutr);
    rFertiliserSurplusPct(C,FertNutr)$rFertReq(C,FertNutr) =
                                              rFertiliserSurplus(C,FertNutr) / rFertReq(C,FertNutr) * 100;
    rFertiliserPerc(C,FertTypeR,FertNutr)$(rFertiliserSurplus(C,FertNutr) + rFertReq(C,FertNutr)) =
                                              rFertiliser(C,FertTypeR,FertNutr) / (rFertiliserSurplus(C,FertNutr) + rFertReq(C,FertNutr)) * 100;

** Animal numbers **

    rNumHerdTypeC(APSLevelHerdType,C)                               = 0;
    rNumProducerC(APS,APSLevel,HerdType,C)$Producer(APS,HerdType)   = 0;
    rNumHerdTypeC(APSLevelHerdType,C)                               = vNumHerdType.l(C,APSLevelHerdType);
    rNumProducerC(APS,APSLevel,HerdType,C)$Producer(APS,HerdType)   = vNumHerdType.l(C,APS,APSLevel,HerdType);

** Animal intake **

    rCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)         = 0;
    rCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)         = vCoProdIntakeHerdTypekgDM.l(C,APSLevelHerdType,ProcOutCP);

    rGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG)           = 0;
    rGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG)           = vGrassIntakeHerdTypekgDM.l(C,APSLevelHerdType,ProcOutG);

* The following is split in two parts: (i) aggregated waste intake and (ii) per-product waste intake
    rAllFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,LossType,ProdAndWaste) = 0;
    rAllFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,LossType,ProdAndWaste)$(WasteSum(LossType) and diag(LossType,ProdAndWaste)) =
        vFoodWasteIntakeHerdTypeSumkgDM.l(C,APSLevelHerdType,LossType);
    rAllFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,LossType,ProdAndWaste)$WasteByProd(LossType) =
        sum(ProcOutH$diag(ProcOutH,ProdAndWaste), vFoodWasteIntakeHerdTypekgDM.l(C,APSLevelHerdType,LossType,ProcOutH));

    rIntakeHerdTypekgDM(C,APSLevelHerdType,ProdAndWaste) = 0;
    rIntakeHerdTypekgDM(C,APSLevelHerdType,ProdAndWaste) =
        sum(ProcOutCP$diag(ProcOutCP,ProdAndWaste), rCoProdIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutCP)) +
        sum(ProcOutG$diag(ProcOutG,ProdAndWaste),   rGrassIntakeHerdTypekgDM(C,APSLevelHerdType,ProcOutG))   +
        sum(LossType, rAllFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,LossType,ProdAndWaste));

    rIntakeHerdTypeFamkgDM(C,APSLevelHerdType,AniProdFamWaste) = 0;
    rIntakeHerdTypeFamkgDM(C,APSLevelHerdType,AniProdFamWaste) =
        sum((ProdAndWaste,sameas(AniProdFam,AniProdFamWaste))$sum(AllProd$diag(AllProd,ProdAndWaste),AniProdFamProd(AniProdFam,AllProd)),
            rIntakeHerdTypekgDM(C,APSLevelHerdType,ProdAndWaste)) +
        sum(sameas(ProdAndWaste,WasteSum), rIntakeHerdTypekgDM(C,APSLevelHerdType,ProdAndWaste))$diag(AniProdFamWaste,'Waste');

* Assign eps to the zeros so they are also written to the worksheets
    rDailyIntakeg(C,ProdFam,ProcOutH)$(FP(ProdFam,ProcOutH) and not rDailyIntakeg(C,ProdFam,ProcOutH))  = eps;
    rDailyIntakeFamilyg(C,ProdFam)$(not rDailyIntakeFamilyg(C,ProdFam))                                 = eps;
    rNutrIntake(C,HumNutr,'Total')$(not rNutrIntake(C,HumNutr,'Total'))                                 = eps;
    rNutrIntake(C,HumNutr,ProdFam)$(not rNutrIntake(C,HumNutr,ProdFam))                                 = eps;
    rNutrIntake(C,HumNutr,'% of min requirement')$(not rNutrIntake(C,HumNutr,'% of min requirement') and HumNutrReq('Min',HumNutr)) = eps;
    rNutrIntake(C,HumNutr,'% of max requirement')$(not rNutrIntake(C,HumNutr,'% of max requirement') and HumNutrReq('Max',HumNutr)) = eps;
    rHumanDiet(C,MinMax,ProdFam)$(not rHumanDiet(C,MinMax,ProdFam) and HumDiet(MinMax,ProdFam))                                     = eps;

$iftheni "%procInOut%" == "on"
    rFoodExports(C,C1)$(not rFoodExports(C,C1))             = eps;
    rFoodExportsPct(C,C1)$(not rFoodExportsPct(C,C1))       = eps;
    rFoodImportsPct(C,C1)$(not rFoodImportsPct(C,C1))       = eps;
    rCoProdExports(C,C1)$(not rCoProdExports(C,C1))         = eps;
    rCoProdExportsPct(C,C1)$(not rCoProdExportsPct(C,C1))   = eps;
    rCoProdImportsPct(C,C1)$(not rCoProdImportsPct(C,C1))   = eps;
$endif

    rCrophaC(Crop,C)$(CropInC(C,Crop) and not rCrophaC(Crop,C))         = eps;
    rCrophaCPct(Crop,C)$(CropInC(C,Crop) and not rCrophaCPct(Crop,C))   = eps;
    rCrophaZ(Crop,C,Z)$(not rCrophaZ(Crop,C,Z))                         = eps;
    rCrophaZPct(Crop,C,Z)$(not rCrophaZPct(Crop,C,Z))                   = eps;
    rLandhaC(LandType,C)$(not rLandhaC(LandType,C))                     = eps;
    rLandhaCPct(LandType,C)$(not rLandhaCPct(LandType,C))               = eps;
    rLandhaZ(LandType,C,Z)$(not rLandhaZ(LandType,C,Z))                 = eps;
    rLandhaZPct(LandType,C,Z)$(not rLandhaZPct(LandType,C,Z))           = eps;
*    rYieldOverall(C,Crop)$(not rYieldOverall(C,Crop))                   = eps;

$iftheni "%cropInOut%" == "on"
    rExportCropLandha(C,C1)$(not rExportCropLandha(C,C1))       = eps;
    rExportCropLandPct(C,C1)$(not rExportCropLandPct(C,C1))     = eps;
$endif

    rFertiliserPerc(C,FertTypeR,FertNutr)$(not rFertiliserPerc(C,FertTypeR,FertNutr))   = eps;
    rFertReq(C,FertNutr)$(not rFertReq(C,FertNutr))                                     = eps;
    rFertiliserSurplus(C,FertNutr)$(not rFertiliserSurplus(C,FertNutr))                 = eps;
    rFertiliserSurplusPct(C,FertNutr)$(not rFertiliserSurplusPct(C,FertNutr))           = eps;

    rNumHerdTypeC(APSLevelHerdType,C)$(not rNumHerdTypeC(APSLevelHerdType,C))                                       = eps;
    rNumProducerC(APSLevelHerdType,C)$(not rNumProducerC(APSLevelHerdType,C))                                       = eps;
    rIntakeHerdTypekgDM(C,APSLevelHerdType,ProdAndWaste)$(not rIntakeHerdTypekgDM(C,APSLevelHerdType,ProdAndWaste)) = eps;
    AniNutrByProductC(C,WasteSum,AniNutr)$(not AniNutrByProductC(C,WasteSum,AniNutr))                               = eps;
    FRVC(C,WasteSum,FertNutr,ConFRV)$(not FRVC(C,WasteSum,FertNutr,ConFRV))                                         = eps;
    GrazingProportion(APS)$(not GrazingProportion(APS))                                                             = eps;
    rNumha(C,Z,Soil,LandType,Crop)                                                                                  = vNumha.l(C,Z,Soil,LandType,Crop);
    rNumha(C,Z,Soil,LandType,Crop)$(not vNumha.l(C,Z,Soil,LandType,Crop))                                           = eps;

    rFoodWasteAv(C,LossType,AllProd)                                                                                = vFoodWasteAv.l(C,LossType,AllProd);
    rFoodWasteAv(C,LossType,AllProd)$(not vFoodWasteAv.l(C,LossType,AllProd))                                       = eps;
    rFertFoodWastekg(C,Crop,WasteByProd,AllProd)                                                                    = vFertFoodWastekg.l(C,Crop,WasteByProd,AllProd);
    rFertFoodWastekg(C,Crop,WasteByProd,AllProd)$(not vFertFoodWastekg.l(C,Crop,WasteByProd,AllProd) and not ProcOutCP(AllProd))
                                                                                        = eps;
    rFertFoodWasteSumkg(C,Crop,WasteSum)                                                = vFertFoodWasteSumkg.l(C,Crop,WasteSum);
    rFertFoodWasteSumkg(C,Crop,WasteSum)$(not vFertFoodWasteSumkg.l(C,Crop,WasteSum))   = eps;
    rFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,WasteByProd,AllProd)                = vFoodWasteIntakeHerdTypekgDM.l(C,APSLevelHerdType,WasteByProd,AllProd);
    rFoodWasteIntakeHerdTypekgDM(C,APSLevelHerdType,WasteByProd,AllProd)$(not vFoodWasteIntakeHerdTypekgDM.l(C,APSLevelHerdType,WasteByProd,AllProd) and not ProcOutCP(AllProd))
                                                                                        = eps;
    rFoodWasteIntakeHerdTypeSumkgDM(C,APSLevelHerdType,WasteSum)                        = vFoodWasteIntakeHerdTypeSumkgDM.l(C,APSLevelHerdType,WasteSum);
    rFoodWasteIntakeHerdTypeSumkgDM(C,APSLevelHerdType,WasteSum)$(not vFoodWasteIntakeHerdTypeSumkgDM.l(C,APSLevelHerdType,WasteSum))
                                                                                        = eps;
    rAnimalCO2eqCapita(C,APSAPSLevel,'MethaneManure')$(not rAnimalCO2eqCapita(C,APSAPSLevel,'MethaneManure'))                           = eps;
    rAnimalCO2eqCapita(C,Rumi,APSLevel,'MethaneEnteric')$(not rAnimalCO2eqCapita(C,Rumi,APSLevel,'MethaneEnteric'))                     = eps;
    rAnimalCO2eqCapita(C,APSAPSLevel,'DirectNitrousOxideManure')$(not rAnimalCO2eqCapita(C,APSAPSLevel,'DirectNitrousOxideManure'))     = eps;
    rAnimalCO2eqCapita(C,APSAPSLevel,'IndirectNitrousOxideManure')$(not rAnimalCO2eqCapita(C,APSAPSLevel,'IndirectNitrousOxideManure')) = eps;
    rAnimalCO2eqCapita(C,APSAPSLevel,'TotalCO2Animal')$(not rAnimalCO2eqCapita(C,APSAPSLevel,'TotalCO2Animal'))                         = eps;
    rCroplandEmissionsCapita(C,Crop,'DirectNitrousOxideManureCropland')$(CropInC(C,Crop) and not rCroplandEmissionsCapita(C,Crop,'DirectNitrousOxideManureCropland')) = eps;
    rCroplandEmissionsCapita(C,Crop,'InDirectNitrousOxideManureCropland')$(CropInC(C,Crop) and not rCroplandEmissionsCapita(C,Crop,'InDirectNitrousOxideManureCropland')) = eps;
    rCroplandEmissionsCapita(C,Crop,'TotalCO2Crop')$(CropInC(C,Crop) and not rCroplandEmissionsCapita(C,Crop,'TotalCO2Crop')) = eps;

    rTotalEmissionsCapita(C,'CO2eqPSF')$(not rTotalEmissionsCapita(C,'CO2eqPSF'))           = eps;
    rTotalEmissionsCapita(C,'CO2eqASF')$(not rTotalEmissionsCapita(C,'CO2eqASF'))           = eps;
    rTotalEmissionsCapita(C,'CO2eqTrans')$(not rTotalEmissionsCapita(C,'CO2eqTrans'))       = eps;
    rTotalEmissionsCapita(C,'CO2eqLUChange')$(not rTotalEmissionsCapita(C,'CO2eqLUChange')) = eps;
    rTotalEmissionsCapita(C,'CO2eqTotal')$(not rTotalEmissionsCapita(C,'CO2eqTotal'))       = eps;

    put_utility "gdxOut" / "%path%CFS-EU-" ActuallyRun.tl:0 "-" Iter.tl:0;
    execute_unload;

* Hack for writing GDX->Excel only (assumes existing output.tmp)
$label writeexcelreport
*$ifthen set writeonly
*sets
*        Scenarios               "Available scenarios"   / MinLand, MinFert, MinGHG, MinPerCapGHG, MinImport /   // no MinImport yet
*        Iter                    "Iteration counter"     / 1*%niter% /
*;
*singleton set ActuallyRun(Scenarios) "Model to actually run" / %model% /;
*loop(Iter,
*$endif

$iftheni.gdxxrwout %excel% == "on"
* Delete output workbook if it exists
    put_utility 'shell' / 'if exist CFS-EU-' ActuallyRun.tl:0 '-' Iter.tl:0 '.xlsx del CFS-EU-' ActuallyRun.tl:0 '-' Iter.tl:0 '.xlsx'
* Write output workbook
    put_utility 'exec' / '%path%gdxxrw.exe i=CFS-EU-' ActuallyRun.tl:0 '-' Iter.tl:0 '.gdx @output.tmp'
$endif.gdxxrwout

* Hack for writing GDX->Excel only
$if set writeonly )
$if set writeonly $goto writeexceliteration

****** Recalculate aggregated values, check for convergence and reset "reporting" variables' attributes ******

    if (ord(iter) <= card(iter),
        AniNutrByProductCIter(Iter+1,C,WasteSum,AniNutr) =
            (
            sum(ProcOutH, vFoodWasteAv.l(C,WasteSum,ProcOutH)*AniNutrByProduct('Foodwaste',ProcOutH,AniNutr)) / sum(ProcOutH, vFoodWasteAv.l(C,WasteSum,ProcOutH))
            )$sum(ProcOutH, vFoodWasteAv.l(C,WasteSum,ProcOutH));
        FeedingLossFractionIter(Iter+1,C) =
            (
            sum((WasteSum,ProcOutH), vFoodWasteAv.l(C,WasteSum,ProcOutH)*LossFraction('Foodwaste',ProcOutH,C,FLoss)) / sum((WasteSum,ProcOutH), vFoodWasteAv.l(C,WasteSum,ProcOutH))
            )$sum((WasteSum,ProcOutH), vFoodWasteAv.l(C,WasteSum,ProcOutH));
        FRVCIter(Iter+1,C,WasteSum,FertNutr,ConFRV) =
            (
            sum(ProcOutH, vFoodWasteAv.l(C,WasteSum,ProcOutH)*FRV('Foodwaste',ProcOutH,FertNutr,ConFRV))   / sum(ProcOutH, vFoodWasteAv.l(C,WasteSum,ProcOutH))
            )$sum(ProcOutH, vFoodWasteAv.l(C,WasteSum,ProcOutH));
        FRVCIter(Iter+1,C,FLoss,FertNutr,ConFRV) =
            (
            sum((APSLevelHerdType,WasteSum),             vFoodWasteIntakeHerdTypeSumkgDM.l(C,APSLevelHerdType,WasteSum)          * FeedingLossFraction(C)                       * FRVC(C,WasteSum,FertNutr,ConFRV)) +
            sum((APSLevelHerdType,WasteByProd,ProcOutH), vFoodWasteIntakeHerdTypekgDM.l(C,APSLevelHerdType,WasteByProd,ProcOutH) * LossFraction('Foodwaste',ProcOutH,C,FLoss)   * FRV('Foodwaste',ProcOutH,FertNutr,ConFRV)) +
            sum((APSLevelHerdType,ProcOutCP),            vCoProdIntakeHerdTypekgDM.l(C,APSLevelHerdType,ProcOutCP)               * LossFraction('Co-Product',ProcOutCP,C,FLoss) * FRV('Co-Product',ProcOutCP,FertNutr,ConFRV))
*no Grass in FRV!            + sum((APSLevelHerdType,ProcOutG),             vGrassIntakeHerdTypekgDM.l(C,APSLevelHerdType,ProcOutG)                 * LossFraction('Grass',ProcOutG,C,FLoss)       * FRV('Grass',ProcOutG,FertNutr,ConFRV))
            ) /
            (
            sum((APSLevelHerdType,WasteSum),             vFoodWasteIntakeHerdTypeSumkgDM.l(C,APSLevelHerdType,WasteSum)          * FeedingLossFraction(C)) +
            sum((APSLevelHerdType,WasteByProd,ProcOutH), vFoodWasteIntakeHerdTypekgDM.l(C,APSLevelHerdType,WasteByProd,ProcOutH) * LossFraction('Foodwaste',ProcOutH,C,FLoss)) +
            sum((APSLevelHerdType,ProcOutCP),            vCoProdIntakeHerdTypekgDM.l(C,APSLevelHerdType,ProcOutCP)               * LossFraction('Co-Product',ProcOutCP,C,FLoss))
*no Grass in FRV!            + sum((APSLevelHerdType,ProcOutG),             vGrassIntakeHerdTypekgDM.l(C,APSLevelHerdType,ProcOutG)                 * LossFraction('Grass',ProcOutG,C,FLoss))
            )$(
            sum((APSLevelHerdType,WasteSum),             vFoodWasteIntakeHerdTypeSumkgDM.l(C,APSLevelHerdType,WasteSum)          * FeedingLossFraction(C)) +
            sum((APSLevelHerdType,WasteByProd,ProcOutH), vFoodWasteIntakeHerdTypekgDM.l(C,APSLevelHerdType,WasteByProd,ProcOutH) * LossFraction('Foodwaste',ProcOutH,C,FLoss)) +
            sum((APSLevelHerdType,ProcOutCP),            vCoProdIntakeHerdTypekgDM.l(C,APSLevelHerdType,ProcOutCP)               * LossFraction('Co-Product',ProcOutCP,C,FLoss))
*no Grass in FRV!            + sum((APSLevelHerdType,ProcOutG),             vGrassIntakeHerdTypekgDM.l(C,APSLevelHerdType,ProcOutG)                 * LossFraction('Grass',ProcOutG,C,FLoss))
            );

** Check for convergence **

        Delta_max(Iter) = 0;
        loop ((C,WasteSum,AniNutr)$(AniNutrByProductC(C,WasteSum,AniNutr) > eps),
            Delta = abs(AniNutrByProductCIter(Iter+1,C,WasteSum,AniNutr)/AniNutrByProductC(C,WasteSum,AniNutr) - 1);
            Delta_max(Iter)$(Delta>Delta_max(Iter)) = Delta;
        );
        loop (C$(FeedingLossFraction(C) > eps),
            Delta = abs(FeedingLossFractionIter(Iter+1,C)/FeedingLossFraction(C) - 1);
            Delta_max(Iter)$(Delta>Delta_max(Iter)) = Delta;
        );
        loop ((C,WasteSum,FertNutr,ConFRV)$(FRVC(C,WasteSum,FertNutr,ConFRV) > eps),
            Delta = abs(FRVCIter(Iter+1,C,WasteSum,FertNutr,ConFRV)/FRVC(C,WasteSum,FertNutr,ConFRV) - 1);
            Delta_max(Iter)$(Delta>Delta_max(Iter)) = Delta;
        );
        loop ((C,FLoss,FertNutr,ConFRV)$(FRVC(C,FLoss,FertNutr,ConFRV) > eps),
            Delta = abs(FRVCIter(Iter+1,C,FLoss,FertNutr,ConFRV)/FRVC(C,FLoss,FertNutr,ConFRV) - 1);
            Delta_max(Iter)$(Delta>Delta_max(Iter)) = Delta;
        );

        break$(Delta_max(Iter) < Delta_eps);

        AniNutrByProductC(C,WasteSum,AniNutr)   = 0;
        FeedingLossFraction(C)                  = 0;
        FRVC(C,WasteSum,FertNutr,ConFRV)        = 0;
        FRVC(C,FLoss,FertNutr,ConFRV)           = 0;
        AniNutrByProductC(C,WasteSum,AniNutr)   = AniNutrByProductCIter(Iter+1,C,WasteSum,AniNutr)$(abs(AniNutrByProductCIter(Iter+1,C,WasteSum,AniNutr)) > eps);
        FeedingLossFraction(C)                  = FeedingLossFractionIter(Iter+1,C)$(abs(FeedingLossFractionIter(Iter+1,C)) > eps);
        FRVC(C,WasteSum,FertNutr,ConFRV)        = FRVCIter(Iter+1,C,WasteSum,FertNutr,ConFRV)$(abs(FRVCIter(Iter+1,C,WasteSum,FertNutr,ConFRV)) > eps);
        FRVC(C,FLoss,FertNutr,ConFRV)           = FRVCIter(Iter+1,C,FLoss,FertNutr,ConFRV)$(abs(FRVCIter(Iter+1,C,FLoss,FertNutr,ConFRV)) > eps);

    );
);

Delta_last = sum(Iter$(ord(Iter) = Current_iter), Delta_max(Iter));

****** Create iteration report ******

file opt /iter_rep.tmp/ ;
put opt, '* Option file for iteration report' /;
$onput
EpsOut=0
  text="Iteration summary"              rng=Summary!A1
  text="Settings:"                      rng=Summary!A3
   par=Delta_eps                        rng=Summary!A4
textID=Delta_eps                        rng=Summary!B4
$offput

put '  text=%niter%                          rng=Summary!A5'

$onput
  text="Maximum number of iterations"   rng=Summary!B5
  text="Reason for termination:"        rng=Summary!A7
$offput

if (Current_iter < %niter% or (Current_iter = %niter% and sum(Iter$(ord(Iter)=card(Iter)), Delta_max(Iter)) < Delta_eps),
    put '  text="Delta stopping criterion reached"      rng=Summary!A8' /
else
    put '  text="Maximum number of iterations reached"  rng=Summary!A8' /
)

$onput
   par=Current_iter                     rng=Summary!A9
textID=Current_iter                     rng=Summary!B9
   par=Delta_last                       rng=Summary!A10
textID=Delta_last                       rng=Summary!B10
textID=Delta_max                        rng=Summary!A12
   par=Delta_max                        rng=Summary!A13                 rdim=1
textID=AniNutrByProductCIter            rng=AniNutrByProductC!A1
   par=AniNutrByProductCIter            rng=AniNutrByProductC!A2        rdim=1 cdim=3
textID=FeedingLossFractionIter          rng=FeedingLossFraction!A1
   par=FeedingLossFractionIter          rng=FeedingLossFraction!A2      rdim=1 cdim=1
textID=FRVCIter                         rng=FRVC!A1
   par=FRVCIter                         rng=FRVC!A2                     rdim=1 cdim=4
$offput
putclose;

put_utility "gdxOut" / "%path%CFS-EU-" ActuallyRun.tl:0 "-IterationReport";
execute_unload Delta_max, Delta_last, Delta_eps, Current_iter, AniNutrByProductCIter, FeedingLossFractionIter, FRVCIter;

* Hack for writing GDX->Excel only
$label writeexceliteration

$iftheni.gdxxrwout %excel% == "on"
* Delete output workbook if it exists
    put_utility 'shell' / 'if exist CFS-EU-' ActuallyRun.tl:0 '-IterationReport.xlsx del CFS-EU-' ActuallyRun.tl:0 '-IterationReport.xlsx'
* Write output workbook
    put_utility 'exec' / '%path%gdxxrw.exe i=CFS-EU-' ActuallyRun.tl:0 '-IterationReport.gdx @iter_rep.tmp'
$endif.gdxxrwout


$ontext
Implemented the following iteration logic:
Iterate over set Iter and stop if either of the following is the case:
1. maximum number of iterations reached (i.e., number of elements of set Iter)
2. Update the iteratively calculated parameters by less than Delta_eps (fractional value)
Create a workbook CFS-EU-NinLand-IterationReport.xlsx
$offtext
